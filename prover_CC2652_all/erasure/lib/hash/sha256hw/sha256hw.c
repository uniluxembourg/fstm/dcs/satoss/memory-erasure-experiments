#include <sha256hw/sha256hw.h>


SHA2_Handle sha256hw_handle;
int_fast16_t hashResult;
void sha256hw(uint8_t *output, uint8_t *input, uint8_t len){
    sha256hw_handle = SHA2_open(0, NULL);
    hashResult = SHA2_hashData(sha256hw_handle, input, len, output);
    SHA2_close(sha256hw_handle);
}

