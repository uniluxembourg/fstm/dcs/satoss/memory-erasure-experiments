#ifndef SHA256HW_H_
#define SHA256HW_H_

#include <stdint.h>
#include "string.h"
#include "constants.h"
#include <ti/drivers/SHA2.h>

void sha256hw(uint8_t *output, uint8_t *input, uint8_t len);

#endif /* SHA256HW_H_ */
