#ifndef HASHHELPER_H_
#define HASHHELPER_H_

#include <stdint.h>
#include <string.h>
#include "constants.h"




#if (HASH_ID == 0)
#include "SHA_256/sha2.h"
#endif

#if (HASH_ID == 1)
#include "blake2/blake2.h"
#endif

#if (HASH_ID == 2)
#include "blake3/blake3.h"
#endif

#if (HASH_ID == 3)
#include "ascon/crypto_hash.h"
#endif

#if (HASH_ID == 4)
#include "aeshash/aeshash.h"
#endif

#if (HASH_ID == 5)
#include "sha256hw/sha256hw.h"
#endif

#if (HASH_ID == 6)
#include "emptyhash/emptyhash.h"
#endif


void h_name(char* name);
void h(uint8_t *output, uint8_t *input, uint8_t len);


#endif /* HASHHELPER_H_ */

