#include "aeshash.h"

#define BLOCK_SIZE (256 / 8)
uint8_t h_i[BLOCK_SIZE];
uint8_t _tmp[BLOCK_SIZE];

uint8_t padded_data[4 * BLOCK_SIZE];
uint8_t padded_len;

AESCBC_Handle handle;
CryptoKey aes_cryptoKey;
int_fast16_t encryptionResult;
uint8_t iv[16] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

void pkcs7_pad(uint8_t* data, uint8_t data_len) {
    uint8_t padding = BLOCK_SIZE - (data_len % BLOCK_SIZE);
    uint8_t padded_size = data_len + padding;

    memcpy(padded_data, data, data_len);
    memset(padded_data + data_len, padding, padding);

    padded_len = padded_size;
}

void xor(uint8_t* a, uint8_t* b, uint8_t len){
    uint8_t j;
    for(j = 0; j < len; j++){
        a[j] ^= b[j];
    }
}

AESCBC_Operation operation;
void aes128_cbc_encrypt(uint8_t *output, uint8_t *input, uint8_t* key, uint8_t len_blocks){
    handle = AESCBC_open(0, NULL);
    // if (handle == NULL);
    CryptoKeyPlaintext_initKey(&aes_cryptoKey, key, BLOCK_SIZE);

    AESCBC_Operation_init(&operation);
    operation.key               = &aes_cryptoKey;
    operation.input             = input;
    operation.output            = output;
    operation.inputLength       = len_blocks * BLOCK_SIZE;
    operation.iv                = iv;
    encryptionResult = AESCBC_oneStepEncrypt(handle, &operation);
    // if (encryptionResult != AESCBC_STATUS_SUCCESS);
    AESCBC_close(handle);
}



void aeshash(uint8_t *output, uint8_t *input, uint8_t len){
    uint8_t i;
    pkcs7_pad(input, len);

    memset(h_i, 0xff, BLOCK_SIZE);
    for(i = 0; i < padded_len; i+= BLOCK_SIZE){
        aes128_cbc_encrypt(_tmp, h_i, padded_data + i, 2);
        xor(h_i, _tmp, BLOCK_SIZE);
    }
    memcpy(_tmp, padded_data + padded_len - BLOCK_SIZE, BLOCK_SIZE);

    xor(_tmp, h_i, BLOCK_SIZE);
    aes128_cbc_encrypt(_tmp, h_i, _tmp, 2);
    xor(h_i, _tmp, BLOCK_SIZE);

    memcpy(output, h_i, BLOCK_SIZE);
}

