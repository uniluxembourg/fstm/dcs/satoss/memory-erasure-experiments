#ifndef AESHASH_H_
#define AESHASH_H_

#include <stdint.h>
#include "string.h"
#include "constants.h"
#include <ti/drivers/AESCBC.h>
#include <ti/drivers/cryptoutils/cryptokey/CryptoKeyPlaintext.h>

void aeshash(uint8_t *output, uint8_t *input, uint8_t len);

#endif /* AESHASH_H_ */
