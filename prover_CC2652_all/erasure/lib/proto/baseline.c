#include "baseline.h"
uint8_t baseline_mem[MEM_SIZE * WORD_SIZE];





uint8_t baseline_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 1;
    uint32_t i;
    for(i = 1; i < MEM_SIZE * WORD_SIZE; i++)   baseline_mem[i] = baseline_mem[i - 1] + 1;
    h(baseline_mem, baseline_mem + 3 * WORD_SIZE, WORD_SIZE);
    return finished;
}
