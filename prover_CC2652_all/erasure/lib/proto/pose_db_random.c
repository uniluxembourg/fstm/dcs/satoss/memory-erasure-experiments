#include "pose_db_random.h"








const uint16_t pose_db_random_rounds = 71;
const uint8_t pose_db_random_msg1[] = ":pose_db_random:\n", pose_db_random_n_msg1 = 17;

const uint8_t pose_db_random_msg2[] = ":ready:\n", pose_db_random_n_msg2 = 8;

uint8_t pose_db_random_mem[MEM_SIZE * WORD_SIZE];

uint16_t pose_db_random_state = 0;
uint8_t pose_db_random_send[WORD_SIZE];
uint8_t pose_db_random_recv[WORD_SIZE];




#if (NO_COMPUTE == 0)

uint8_t pose_db_random_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    uint16_t q;
    *performance_mode = 0;
    if(pose_db_random_state == 0){
        if(*recv_addr != NULL)finished = 2;
        else{
            *recv_addr = pose_db_random_recv;
            *recv_length = pose_db_random_n_msg1;
            *send_addr = NULL;
        }
    }
    else if(pose_db_random_state >= 1 && pose_db_random_state <= MEM_SIZE){

       if(pose_db_random_state == 1 && memcmp(pose_db_random_recv, pose_db_random_msg1, pose_db_random_n_msg1 ) != 0)finished = 2;
       else{
           *recv_addr = pose_db_random_mem + (uint32_t)(pose_db_random_state - 1) * WORD_SIZE;
           *recv_length = WORD_SIZE;
           *send_addr = NULL;
       }
    }
    else if(pose_db_random_state == MEM_SIZE + 1){

        *recv_addr = pose_db_random_recv;
        *recv_length = 2;
        memcpy(pose_db_random_send,pose_db_random_msg2, pose_db_random_n_msg2);
        *send_addr = pose_db_random_send;
        *send_length = pose_db_random_n_msg2;
    }
    else if(pose_db_random_state <= MEM_SIZE + pose_db_random_rounds){

        q = bytes2int(pose_db_random_recv);

        *recv_addr = pose_db_random_recv;
        *recv_length = 2;
        *send_addr = pose_db_random_mem + q * WORD_SIZE;
        *send_length = WORD_SIZE;
    }
    else if(pose_db_random_state == MEM_SIZE + pose_db_random_rounds + 1){
        q = bytes2int(pose_db_random_recv);
        *send_addr = pose_db_random_mem + q * WORD_SIZE;
        *send_length = WORD_SIZE;
        *recv_addr = NULL;
        finished = 1;
    }
    pose_db_random_state += 1;
    if(finished != 0){
        memset(pose_db_random_recv, 0, WORD_SIZE);
        memset(pose_db_random_send, 0, WORD_SIZE);
        pose_db_random_state = 0;
    }
    return finished;
}



#else

uint8_t pose_db_random_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    *performance_mode = 0;
    if(pose_db_random_state == 0){
        *recv_addr = pose_db_random_recv;
        *recv_length = pose_db_random_n_msg1;
        *send_addr = NULL;
    }
    else if(pose_db_random_state >= 1 && pose_db_random_state <= MEM_SIZE){
       *recv_addr = pose_db_random_mem;
       *recv_length = WORD_SIZE;
       *send_addr = NULL;
    }
    else if(pose_db_random_state == MEM_SIZE + 1){
        *recv_addr = pose_db_random_recv;
        *recv_length = 2;
        *send_addr = (uint8_t*)pose_db_random_msg2;
        *send_length = pose_db_random_n_msg2;
    }
    else if(pose_db_random_state <= MEM_SIZE + pose_db_random_rounds){
        *recv_addr = pose_db_random_recv;
        *recv_length = 2;
        *send_addr = pose_db_random_mem;
        *send_length = WORD_SIZE;
    }
    else if(pose_db_random_state == MEM_SIZE + pose_db_random_rounds + 1){
        *send_addr = pose_db_random_mem;
        *send_length = WORD_SIZE;
        *recv_addr = NULL;
        finished = 1;
    }
    pose_db_random_state += 1;
    if(finished != 0){
        pose_db_random_state = 0;
    }
    return finished;
}


#endif






