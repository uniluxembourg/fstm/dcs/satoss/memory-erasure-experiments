#ifndef KARVELAS_H_

#define KARVELAS_H_

#include <string.h>
#include <stdint.h>
#include "constants.h"
#include "hashhelper.h"

void karvelas_run(uint8_t n, uint8_t *mem_start, uint8_t *challenge);
uint8_t karvelas_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode);

#endif /* KARVELAS_H_ */
