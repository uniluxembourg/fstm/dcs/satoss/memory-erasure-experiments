#ifndef PERITO_H_

#define PERITO_H_

#include <string.h>
#include <stdint.h>
#include "constants.h"
#include "hashhelper.h"

void perito_run(uint32_t m, uint8_t *mem_start, uint8_t *challenge);
uint8_t perito_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode);

#endif /* PERITO_H_ */
