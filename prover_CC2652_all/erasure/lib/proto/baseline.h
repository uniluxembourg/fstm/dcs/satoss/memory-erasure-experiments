#ifndef BASELINE_H_

#define BASELINE_H_
#include <stdint.h>
#include "constants.h"
#include "hashhelper.h"
uint8_t baseline_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode);

#endif /* BASELINE_H_ */
