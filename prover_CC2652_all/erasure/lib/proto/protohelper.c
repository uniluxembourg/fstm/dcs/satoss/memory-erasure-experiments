#include "protohelper.h"



uint8_t run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished;
    #if (PROTOCOL_ID == 0)
        finished = pose_db_run_protocol_prover(recv_addr, recv_length, send_addr, send_length, performance_mode);
    #endif
    #if (PROTOCOL_ID == 1)
        finished = dziembowski_run_protocol_prover(recv_addr, recv_length, send_addr, send_length, performance_mode);
    #endif
    #if (PROTOCOL_ID == 2)
        finished = baseline_run_protocol_prover(recv_addr, recv_length, send_addr, send_length, performance_mode);
    #endif
    #if (PROTOCOL_ID == 3)
        finished = karvelas_run_protocol_prover(recv_addr, recv_length, send_addr, send_length, performance_mode);
    #endif
    #if (PROTOCOL_ID == 4)
        finished = karame_run_protocol_prover(recv_addr, recv_length, send_addr, send_length, performance_mode);
    #endif
    #if (PROTOCOL_ID == 5)
        finished = perito_run_protocol_prover(recv_addr, recv_length, send_addr, send_length, performance_mode);
    #endif
    #if (PROTOCOL_ID == 6)
        finished = pose_db_light_run_protocol_prover(recv_addr, recv_length, send_addr, send_length, performance_mode);
    #endif
    #if (PROTOCOL_ID == 7)
        finished = pose_db_random_run_protocol_prover(recv_addr, recv_length, send_addr, send_length, performance_mode);
    #endif
    return finished;
}
