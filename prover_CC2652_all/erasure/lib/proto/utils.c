#include "utils.h"



// just 4 bytes
void int2bytes(uint32_t a, uint8_t* b){
  memcpy(b, &a, 4);
}

void bytes_xor(uint8_t* a, uint8_t* b, uint8_t n, uint8_t* res){
  uint8_t i;
  for(i = 0; i < n; i++)
    res[i] = a[i] ^ b[i];
}

// uses little endian for 'a'
void bytes_cyclic_shift(uint8_t* a, uint8_t n, uint8_t c, uint8_t* res){
  uint16_t i, ic;
  memset(res, 0, n);
  for(i = 0; i < (uint16_t)n * 8 - c; i++){
    ic = i + c;
    if( a[n - 1 - i / 8] & (1 << (7 - i % 8)))
      res[n - 1 - ic / 8] ^= (1 << (7 - ic % 8));
  }
  for(i = 0; i < c; i++){
    ic = (uint16_t)8 * n - c + i;
    if( a[n - 1 - ic / 8] & (1 << (7 - ic % 8)))
      res[n - 1 - i / 8] ^= (1 << (7 - i % 8));
  }
}


uint8_t bits2int(uint8_t* a, uint32_t start, uint32_t end){
  uint32_t i;
  uint8_t res;
  res = 0;
  for(i = start; i < end; i++){
    res = (res << 1) + ((a[i / 8] & (1 << (i % 8))) > 0 ? 1 : 0);
  }
  return res;
}


uint16_t bytes2int(uint8_t* x){
    return x[0] + ((uint16_t)x[1] << 8);
}
