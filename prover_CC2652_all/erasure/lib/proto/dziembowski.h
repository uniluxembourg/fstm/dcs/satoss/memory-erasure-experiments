#ifndef DZIEMBOWSKI_H_

#define DZIEMBOWSKI_H_

#include <string.h>
#include <stdint.h>
#include "constants.h"
#include "hashhelper.h"

void dziembowski_run(uint8_t n, uint8_t *mem_start, uint8_t *challenge);
uint8_t dziembowski_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode);


#endif /* DZIEMBOWSKI_H_ */
