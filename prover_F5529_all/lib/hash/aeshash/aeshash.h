#ifndef AESHASH_H_
#define AESHASH_H_


#include "driverlib.h"
#include "string.h"
#include "constants.h"

void aeshash(uint8_t *output, uint8_t *input, uint8_t len);

#endif /* AESHASH_H_ */
