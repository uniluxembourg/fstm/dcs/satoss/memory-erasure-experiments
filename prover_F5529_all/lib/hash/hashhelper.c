
#include "hashhelper.h"




#if (HASH_ID == 0)
void convert_endian(uint8_t* mem, uint8_t len){
  uint8_t tmp;
  uint8_t i;
  for(i = 0; i < len; i+= 4){
    tmp = mem[i + 3];
    mem[i + 3] = mem[i];
    mem[i] = tmp;

    tmp = mem[i + 2];
    mem[i + 2] = mem[i + 1];
    mem[i + 1] = tmp;
  }
}

void sha256_h(uint8_t *output, uint8_t *input, uint8_t len){
  short hash_mode;
  uint8_t M[2 * 2 * WORD_SIZE];
  hash_mode = 0x1; // SHA_256

  memcpy(M, input, len);
  memset(M, 0, (len + 3) / 4 * 4 - len);
  convert_endian(M, (len + 3) / 4 * 4);

  SHA_2( (uint32_t*)M, len, (uint32_t*)output, hash_mode);

  convert_endian(output, WORD_SIZE);
}
#endif

#if (HASH_ID == 1)
void blake2_h(uint8_t *output, uint8_t *input, uint8_t len){
    blake2s( output, BLAKE2S_OUTBYTES, input, len, NULL, 0 );
}
#endif


#if (HASH_ID == 2)
void blake3_h(uint8_t *output, uint8_t *input, uint8_t len){
    blake3_hasher hasher;
    blake3_hasher_init(&hasher);
    blake3_hasher_update(&hasher, input, len);
    blake3_hasher_finalize(&hasher, output, BLAKE3_OUT_LEN);
}
#endif

#if (HASH_ID == 3)
void ascon_h(uint8_t *output, uint8_t *input, uint8_t len){
    crypto_hash(output, input, len);
}
#endif



#if (HASH_ID == 4)

void aes_h(uint8_t *output, uint8_t *input, uint8_t len){
    aeshash(output, input, len);
}
#endif

void h_name(char* name){
  switch (HASH_ID)
  {
  case 0:
    strcpy(name, "sha256");
    break;
  case 1:
    strcpy(name, "blake2");
    break;
  case 2:
    strcpy(name, "blake3");
    break;
  case 3:
    strcpy(name, "ascon");
    break;
  case 4:
      strcpy(name, "aeshash");
      break;
  default:
    break;
  }
}

void h(uint8_t *output, uint8_t *input, uint8_t len){

#if (HASH_ID == 0)
    sha256_h(output, input, len);
#endif
#if (HASH_ID == 1)
    blake2_h(output, input, len);
#endif
#if (HASH_ID == 2)
    blake3_h(output, input, len);
#endif
#if (HASH_ID == 3)
    ascon_h(output, input, len);
#endif
#if (HASH_ID == 4)
    aes_h(output, input, len);
#endif
#if (HASH_ID == 5)
    sha256hw_h(output, input, len);
#endif
#if (HASH_ID == 6)
    emptyhash(output, input, len);
#endif
}
