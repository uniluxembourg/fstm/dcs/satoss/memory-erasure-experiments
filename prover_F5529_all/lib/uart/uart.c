
#include "uart.h"

//Receive buffer for the uart, incoming data needs a place to go to otherwise they will be overwritten.
uint8_t UartRcvBuf[RXBUF_SIZE];

//Index of the uartrcvbuf so that no data is overwritten
uint8_t UartRcvBufIndex = 0;

uint8_t last_end_line = 0;

#ifdef FRAM_DEVICE

//Initializes the Uart
void UartInit(void)
{
    // CS_getSMCLK(); this gives 1M
    GPIO_setAsPeripheralModuleFunctionInputPin(
        GPIO_PORT_P6,
        GPIO_PIN0 + GPIO_PIN1,
        GPIO_PRIMARY_MODULE_FUNCTION
        );
    EUSCI_A_UART_initParam param = {0};
    param.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
    param.clockPrescalar = 6;
    param.firstModReg = 8;
    param.secondModReg = 17;
    param.parity = EUSCI_A_UART_NO_PARITY;
    param.msborLsbFirst = EUSCI_A_UART_LSB_FIRST;
    param.numberofStopBits = EUSCI_A_UART_ONE_STOP_BIT;
    param.uartMode = EUSCI_A_UART_MODE;
    param.overSampling = EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION;

    if (STATUS_FAIL == EUSCI_A_UART_init(EUSCI_A3_BASE, &param)) {
        return;
    }

    EUSCI_A_UART_enable(EUSCI_A3_BASE);

    EUSCI_A_UART_clearInterrupt(EUSCI_A3_BASE,
      EUSCI_A_UART_RECEIVE_INTERRUPT);

    // Enable USCI_A3 RX interrupt
    EUSCI_A_UART_enableInterrupt(EUSCI_A3_BASE,
      EUSCI_A_UART_RECEIVE_INTERRUPT);                     // Enable interrupt

    __enable_interrupt();
}


void UartSend(uint8_t* buf, uint8_t len, uint8_t end_line)
{
    uint8_t i = 0;

    if(end_line)buf[len++] = '\n';

    while(i < len)
    {
        //Put the bit into the Transmit buffer, then increment i
        //UCA3TXBUF = *(buf+(i++));

        //Wait for each bit
        //while(!(UCTXIFG==(UCTXIFG & UCA0IFG))&&((UCA3STAT & UCBUSY) == UCBUSY));

        EUSCI_A_UART_transmitData(EUSCI_A3_BASE,buf[i++]);
    }
}



uint8_t UartReceive(uint8_t* buf, uint8_t n)
{
    uint8_t i, count = 0;
    if(n == 0 && last_end_line != 0){
        //UCA3IE &= ~UCRXIE;
        EUSCI_A_UART_disableInterrupt(EUSCI_A3_BASE,
              EUSCI_A_UART_RECEIVE_INTERRUPT);

        //Put each bit of the Receive buffer into another buffer to save it
        for(i = 0; UartRcvBuf[i] != '\n'; i++) {
            buf[i] = UartRcvBuf[i];
        }
        count = i;

        for(i = count + 1; i < UartRcvBufIndex; i++)
            UartRcvBuf[i - count - 1] = UartRcvBuf[i];
        UartRcvBufIndex -= count + 1;
        last_end_line -= count + 1;
        //UCA3IE |= UCRXIE;
        EUSCI_A_UART_enableInterrupt(EUSCI_A3_BASE,
              EUSCI_A_UART_RECEIVE_INTERRUPT);
    }
    else if(n > 0 && UartRcvBufIndex >= n){
        //UCA3IE &= ~UCRXIE;
        EUSCI_A_UART_disableInterrupt(EUSCI_A3_BASE,
              EUSCI_A_UART_RECEIVE_INTERRUPT);

        for(i = 0; i < n; i++) {
            buf[i] = UartRcvBuf[i];
        }
        count = n;

        for(i = count; i < UartRcvBufIndex; i++)
            UartRcvBuf[i - count] = UartRcvBuf[i];
        UartRcvBufIndex -= n;

        if(last_end_line >= n) last_end_line -= n;
        else last_end_line = 0;

        //UCA3IE |= UCRXIE;
        EUSCI_A_UART_enableInterrupt(EUSCI_A3_BASE,
              EUSCI_A_UART_RECEIVE_INTERRUPT);

    }
    return count;
}


//******************************************************************************
//
//This is the USCI_A3 interrupt vector service routine.
//
//******************************************************************************
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A3_VECTOR
__interrupt
#endif
void USCI_A3_ISR(void)
{
  switch(__even_in_range(UCA3IV,USCI_UART_UCTXCPTIFG))
  {
    case USCI_NONE: break;
    case USCI_UART_UCRXIFG:
        if(UartRcvBufIndex < RXBUF_SIZE){
            UartRcvBuf[UartRcvBufIndex] = EUSCI_A_UART_receiveData(EUSCI_A3_BASE); //fetch byte and store
            if(UartRcvBuf[UartRcvBufIndex] == '\n')last_end_line = UartRcvBufIndex + 1;
            UartRcvBufIndex++;
        }
      break;
    case USCI_UART_UCTXIFG: break;
    case USCI_UART_UCSTTIFG: break;
    case USCI_UART_UCTXCPTIFG: break;
  }
}
#endif

#ifdef FLASH_DEVICE

// Default Baud rate set is 9600
#define UCA0_OS   1    // 1 = oversampling mode, 0 = low-freq mode
#define UCA0_BR0  6  // Value of UCA0BR0 register                    1MHz / 16 / 9600 (see User's Guide)
#define UCA0_BR1  0    // Value of UCA0BR1 register
#define UCA0_BRS  UCBRS_0    // Value of UCBRS field in UCA0MCTL register    Modulation UCBRSx=0, UCBRFx=13
#define UCA0_BRF  UCBRF_13    // Value of UCBRF field in UCA0MCTL register

//Initializes the Uart
void UartInit(void)
{
        P3SEL |= BIT3+BIT4;                          // P3.3,4 = USCI_A0 TXD/RXD
        UCA0CTL1 |= UCSWRST;                         // **Put state machine in reset**
        UCA0CTL1 |= UCSSEL_2;                        // SMCLK
        UCA0BR0 = UCA0_BR0;
        UCA0BR1 = UCA0_BR1;
        UCA0MCTL |= UCA0_OS | UCA0_BRF | UCA0_BRS;   // Modulation
        UCA0CTL1 &= ~UCSWRST;                        // **Initialize USCI state machine**
        UCA0IE |= UCRXIE;                            // Enable USCI_A0 RX interrupt
        __enable_interrupt();                        // Enable interrupts globally
}


void UartSend(uint8_t* buf, uint8_t len, uint8_t end_line)
{
    uint8_t i = 0;

    if(end_line)buf[len++] = '\n';

    while(i < len)
    {
        //Put the bit into the Transmit buffer, then increment i
        UCA0TXBUF = *(buf+(i++));

        //Wait for each bit
        while(!(UCTXIFG==(UCTXIFG & UCA0IFG))&&((UCA0STAT & UCBUSY) == UCBUSY));
    }
}



uint8_t UartReceive(uint8_t* buf, uint8_t n)
{
    uint8_t i, count = 0;
    if(n == 0 && last_end_line != 0){
        UCA0IE &= ~UCRXIE;

        //Put each bit of the Receive buffer into another buffer to save it
        for(i = 0; UartRcvBuf[i] != '\n'; i++) {
            buf[i] = UartRcvBuf[i];
        }
        count = i;

        for(i = count + 1; i < UartRcvBufIndex; i++)
            UartRcvBuf[i - count - 1] = UartRcvBuf[i];
        UartRcvBufIndex -= count + 1;
        last_end_line -= count + 1;
        UCA0IE |= UCRXIE;
    }
    else if(n > 0 && UartRcvBufIndex >= n){
        UCA0IE &= ~UCRXIE;

        for(i = 0; i < n; i++) {
            buf[i] = UartRcvBuf[i];
        }
        count = n;

        for(i = count; i < UartRcvBufIndex; i++)
            UartRcvBuf[i - count] = UartRcvBuf[i];
        UartRcvBufIndex -= n;

        if(last_end_line >= n) last_end_line -= n;
        else last_end_line = 0;

        UCA0IE |= UCRXIE;

    }
    return count;
}


#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(USCI_A0_VECTOR))) USCI_A0_ISR (void)
#else
#error Compiler not supported!
#endif
{
    switch(__even_in_range(UCA0IV,4))
          {
            case 0:break;
            case 2:
                if(UartRcvBufIndex < RXBUF_SIZE){
                    UartRcvBuf[UartRcvBufIndex] = UCA0RXBUF; //fetch byte and store
                    if(UartRcvBuf[UartRcvBufIndex] == '\n')last_end_line = UartRcvBufIndex + 1;
                    UartRcvBufIndex++;
                }

             break;
            case 4: break;
            default: break;
          }
}
#endif
