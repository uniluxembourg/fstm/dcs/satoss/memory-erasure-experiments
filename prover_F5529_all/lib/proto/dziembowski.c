#include "dziembowski.h"



uint32_t dziembowski_cid;
uint8_t dziembowski_input[3 * WORD_SIZE + 4 + WORD_SIZE];

uint8_t dziembowski_mem[MEM_SIZE * WORD_SIZE];
const uint8_t dziembowski_msg1[] = ":dziembowski:\n", dziembowski_n_msg1 = 14;

uint16_t dziembowski_state = 0;
uint8_t dziembowski_send[WORD_SIZE];
uint8_t dziembowski_recv[WORD_SIZE];
uint8_t dziembowski_challenge[WORD_SIZE];


void dziembowski_update_label_noid(uint8_t* addr, uint8_t* pred0, uint8_t* pred1){
  uint8_t len;


  
  memcpy(dziembowski_input + WORD_SIZE, &dziembowski_cid, 4);
  len = WORD_SIZE + 4;
  if(pred0 != NULL){
    memcpy(dziembowski_input + WORD_SIZE + 4, pred0, WORD_SIZE);
    len += WORD_SIZE;
  }
  if(pred1 != NULL){
    memcpy(dziembowski_input + 2 * WORD_SIZE + 4, pred1, WORD_SIZE);
    len += WORD_SIZE;
  }

  dziembowski_cid++;

  h(addr, dziembowski_input, len);
}




void dziembowski_apply_layer(uint32_t n, uint8_t *mem_start){
  uint32_t i;
  for(i = 0; i < n - 1; i++){
    dziembowski_update_label_noid(mem_start + i * WORD_SIZE, mem_start + i * WORD_SIZE, mem_start + (i + 1) * WORD_SIZE);
  }
}



void dziembowski_fill(uint8_t n, uint8_t *mem_start, uint8_t *challenge){
  uint32_t n2, j;

  n2 = (uint32_t) 1 << n;
  memcpy(dziembowski_input, challenge, WORD_SIZE);
  dziembowski_cid = 0;
  for(j = 0; j < n2; j++){
    dziembowski_update_label_noid(mem_start + j * WORD_SIZE, NULL, NULL);
  }

  for(j = 0; j < n2 - 1; j++){
    dziembowski_apply_layer(n2 - j, mem_start);
  }
  

}



void dziembowski_run(uint8_t n, uint8_t *mem_start, uint8_t *challenge){
  dziembowski_fill(n, mem_start, challenge);
}

#if (NO_COMPUTE == 0)

uint8_t dziembowski_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    *performance_mode = 0;
    if(dziembowski_state == 0){
        if(*recv_addr != NULL)finished = 2;
        else{
            *recv_addr = dziembowski_recv;
            *recv_length = dziembowski_n_msg1;
            *send_addr = NULL;
        }
    }
    else if(dziembowski_state == 1){
       if(memcmp(dziembowski_recv, dziembowski_msg1, dziembowski_n_msg1 ) != 0)finished = 2;
       else{
           *recv_addr = dziembowski_challenge;
           *recv_length = WORD_SIZE;
           *send_addr = NULL;
           *performance_mode = 1;
       }
    }
    else if(dziembowski_state == 2){

        dziembowski_run(MEM_SIZE_LOG, dziembowski_mem, dziembowski_challenge);

        *recv_addr = NULL;
        *send_addr = dziembowski_mem;
        *send_length = WORD_SIZE;
        finished = 1;
    }
    dziembowski_state += 1;
    if(finished != 0){
        memset(dziembowski_recv, 0, WORD_SIZE);
        memset(dziembowski_send, 0, WORD_SIZE);
        dziembowski_state = 0;
    }
    return finished;
}

#else

uint8_t dziembowski_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    *performance_mode = 0;
    if(dziembowski_state == 0){
        *recv_addr = dziembowski_recv;
        *recv_length = dziembowski_n_msg1;
        *send_addr = NULL;
    }
    else if(dziembowski_state == 1){
       *recv_addr = dziembowski_challenge;
       *recv_length = WORD_SIZE;
       *send_addr = NULL;
    }
    else if(dziembowski_state == 2){
        *recv_addr = NULL;
        *send_addr = dziembowski_mem;
        *send_length = WORD_SIZE;
        finished = 1;
    }
    dziembowski_state += 1;
    if(finished != 0){
        dziembowski_state = 0;
    }
    return finished;
}

#endif



