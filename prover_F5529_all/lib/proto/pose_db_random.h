#ifndef POSE_DB_RANDOM_H_
#define POSE_DB_RANDOM_H_


#include <string.h>
#include <stdint.h>
#include "constants.h"
#include "hashhelper.h"
#include "utils.h"

uint8_t pose_db_random_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode);

#endif /* POSE_DB_RANDOM_H_ */
