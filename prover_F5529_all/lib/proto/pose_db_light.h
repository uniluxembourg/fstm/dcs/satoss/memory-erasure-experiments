#ifndef POSE_DB_LIGHT_H_
#define POSE_DB_LIGHT_H_


#include <string.h>
#include <stdint.h>
#include "constants.h"
#include "hashhelper.h"
#include "utils.h"

#define SEGMENT_SIZE_LOG 4

void pose_db_light_run(uint8_t n, uint8_t *mem_start, uint8_t *challenge);

uint8_t pose_db_light_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode);

#endif /* POSE_DB_LIGHT_H_ */
