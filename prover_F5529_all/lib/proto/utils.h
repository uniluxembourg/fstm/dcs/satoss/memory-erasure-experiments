#ifndef UTILS_H_

#define UTILS_H_

#include <string.h>
#include <stdint.h>



void int2bytes(uint32_t a, uint8_t* b);

void bytes_xor(uint8_t* a, uint8_t* b, uint8_t n, uint8_t* res);

void bytes_cyclic_shift(uint8_t* a, uint8_t n, uint8_t c, uint8_t* res);


uint8_t bits2int(uint8_t* a, uint32_t start, uint32_t end);

uint16_t bytes2int(uint8_t* x);

#endif /* UTILS_H_ */
