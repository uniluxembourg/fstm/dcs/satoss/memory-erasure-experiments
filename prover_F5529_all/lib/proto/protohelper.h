#ifndef PROTOHELPER_H_
#define PROTOHELPER_H_

#include <stdint.h>

#if (PROTOCOL_ID == 0)
#include "proto/pose_db.h"
#endif

#if (PROTOCOL_ID == 1)
#include "proto/dziembowski.h"
#endif

#if (PROTOCOL_ID == 2)
#include "proto/baseline.h"
#endif

#if (PROTOCOL_ID == 3)
#include "proto/karvelas.h"
#endif

#if (PROTOCOL_ID == 4)
#include "proto/karame.h"
#endif

#if (PROTOCOL_ID == 5)
#include "proto/perito.h"
#endif

#if (PROTOCOL_ID == 6)
#include "proto/pose_db_light.h"
#endif

#if (PROTOCOL_ID == 7)
#include "proto/pose_db_random.h"
#endif


uint8_t run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode);


#endif /* PROTOHELPER_H_ */
