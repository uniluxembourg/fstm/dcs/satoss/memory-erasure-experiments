#include "perito.h"

uint8_t perito_mem[MEM_SIZE * WORD_SIZE];
const uint8_t perito_msg1[] = ":perito:\n", perito_n_msg1 = 9;

uint16_t perito_state = 0;
uint8_t perito_send[WORD_SIZE];
uint8_t perito_recv[WORD_SIZE];

#if (HASH_ID != 6)
    void hmac_sha256(const unsigned char *key, unsigned int key_size,
          const unsigned char *message, unsigned int message_len,
          unsigned char *mac, unsigned mac_size);
#endif


void perito_run(uint32_t m, uint8_t *mem_start, uint8_t *challenge){
    uint8_t key[WORD_SIZE];
    uint8_t res[WORD_SIZE];
    //memcpy(mem_start, challenge, (m - 1) * WORD_SIZE);
    memcpy(key, mem_start + (m - 1) * WORD_SIZE, WORD_SIZE);
    #if (HASH_ID != 6)
    hmac_sha256(key, WORD_SIZE, mem_start, (m - 1) * WORD_SIZE, res, WORD_SIZE);
    #endif
    memcpy(mem_start, res, WORD_SIZE);
}


#if (NO_COMPUTE == 0)

uint8_t perito_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    *performance_mode = 0;
    if(perito_state == 0){
        if(*recv_addr != NULL)finished = 2;
        else{
            *recv_addr = perito_recv;
            *recv_length = perito_n_msg1;
            *send_addr = NULL;
        }
    }
    else if(perito_state >= 1 && perito_state <= MEM_SIZE){
       if(perito_state == 1 && memcmp(perito_recv, perito_msg1, perito_n_msg1 ) != 0)finished = 2;
       else{
           *recv_addr = perito_mem + (uint32_t)(perito_state - 1) * WORD_SIZE;
           *recv_length = WORD_SIZE;
           *send_addr = NULL;
           if(perito_state == MEM_SIZE){
               *performance_mode = 1;
           }
       }
    }
    else if(perito_state == MEM_SIZE + 1){

        perito_run(MEM_SIZE, perito_mem, perito_mem);

        *recv_addr = NULL;
        *send_addr = perito_mem;
        *send_length = WORD_SIZE;
        *performance_mode = 0;
        finished = 1;
    }
    perito_state += 1;
    if(finished != 0){
        memset(perito_recv, 0, WORD_SIZE);
        memset(perito_send, 0, WORD_SIZE);
        perito_state = 0;
    }
    return finished;
}


#else


uint8_t perito_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    *performance_mode = 0;
    if(perito_state == 0){
        *recv_addr = perito_recv;
        *recv_length = perito_n_msg1;
        *send_addr = NULL;
    }
    else if(perito_state >= 1 && perito_state <= MEM_SIZE){
       *recv_addr = perito_mem;
       *recv_length = WORD_SIZE;
       *send_addr = NULL;
    }
    else if(perito_state == MEM_SIZE + 1){
        *recv_addr = NULL;
        *send_addr = perito_mem;
        *send_length = WORD_SIZE;
        finished = 1;
    }
    perito_state += 1;
    if(finished != 0){
        perito_state = 0;
    }
    return finished;
}

#endif
