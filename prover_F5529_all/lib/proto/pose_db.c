#include "pose_db.h"





void pose_db_fill_depth_robust_right(uint8_t n, uint8_t *mem_start);
void pose_db_fill_depth_robust_full(uint8_t n, uint8_t *mem_start, uint8_t first);
void pose_db_fill_depth_robust(uint8_t n, uint8_t *mem_start, uint8_t *challenge, uint8_t initial_cid);

uint32_t pose_db_cid;
uint8_t pose_db_tmp[WORD_SIZE], pose_db_input[3 * WORD_SIZE + 4];



const uint16_t pose_db_rounds = 71;
const uint8_t pose_db_msg1[] = ":pose_db:\n", pose_db_n_msg1 = 10;

const uint8_t pose_db_msg2[] = ":ready:\n", pose_db_n_msg2 = 8;

uint8_t pose_db_mem[MEM_SIZE * WORD_SIZE];

uint16_t pose_db_state = 0;
uint8_t pose_db_send[WORD_SIZE];
uint8_t pose_db_recv[WORD_SIZE];
uint8_t pose_db_challenge[WORD_SIZE];



void pose_db_update_label_noid(uint8_t* addr, uint8_t* pred0, uint8_t* pred1){
  uint8_t len;


  
  memcpy(pose_db_input + WORD_SIZE, &pose_db_cid, 4);
  len = WORD_SIZE + 4;
  if(pred0 != NULL){
    memcpy(pose_db_input + WORD_SIZE + 4, pred0, WORD_SIZE);
    len += WORD_SIZE;
  }
  if(pred1 != NULL){
    memcpy(pose_db_input + 2 * WORD_SIZE + 4, pred1, WORD_SIZE);
    len += WORD_SIZE;
  }

  pose_db_cid++;

  h(addr, pose_db_input, len);
}


void pose_db_update_layer(uint32_t jump, uint32_t n2, uint8_t *mem_start){
  int32_t j, k;

  j = 0;
  while(j < n2){
      for(k = j; k < j + jump; k++){
        pose_db_update_label_noid(pose_db_tmp, mem_start + k * WORD_SIZE, mem_start + (k + jump) * WORD_SIZE);
        
        pose_db_update_label_noid(mem_start + (k + jump) * WORD_SIZE, mem_start + k * WORD_SIZE, mem_start + (k + jump) * WORD_SIZE);

        memcpy(mem_start + k * WORD_SIZE, pose_db_tmp, WORD_SIZE);

      }
      j += 2 * jump;
  }
}


void pose_db_apply_butterfly(uint8_t n, uint8_t *mem_start){
  int32_t i;
  uint32_t n2;

  n2 =  (uint32_t)1 << n;
  for(i = 0; i < n; i++){
      pose_db_update_layer((uint32_t)1 << (n - 1 - i), n2, mem_start);
  }

  for(i = 0; i < n2; i++){
    pose_db_update_label_noid(mem_start + i * WORD_SIZE, mem_start + i * WORD_SIZE, NULL);
  }

  for(i = 0; i < n; i++){
      pose_db_update_layer((uint32_t)1 << i, n2, mem_start);
  }

}


void pose_db_fill_depth_robust_right(uint8_t n, uint8_t *mem_start){
  uint32_t j, jj, n21;
  if(n == 0){
    pose_db_update_label_noid(mem_start, mem_start, NULL);
  }
  else{
    
    
    for(j = 1; j < (1<<n); j++){
      pose_db_update_label_noid(mem_start + j * WORD_SIZE, mem_start + j * WORD_SIZE, NULL);
    }

    jj = 1;
    for(j = 0; j < n; j++){
        pose_db_apply_butterfly(j, mem_start + jj * WORD_SIZE);
        jj += (1 << j);
    }

    pose_db_fill_depth_robust_full(n - 1, mem_start, 0);
    
    n21 = (uint32_t)1 << (n - 1);
    for(j = 0; j < n21; j++){
      pose_db_update_label_noid(mem_start + (n21 + j) * WORD_SIZE, mem_start + j * WORD_SIZE, mem_start + (n21 + j) * WORD_SIZE);
    }
    
    pose_db_apply_butterfly(n - 1, mem_start + n21 * WORD_SIZE);

    pose_db_fill_depth_robust_right(n - 1, mem_start + n21 * WORD_SIZE);
  }
}

void pose_db_fill_depth_robust_full(uint8_t n, uint8_t *mem_start, uint8_t first){
  uint32_t j, n21;

  if(n == 0){
      if(first){
          pose_db_update_label_noid(mem_start, NULL, NULL);
      }
      else{
          pose_db_update_label_noid(mem_start, mem_start, NULL);
      }
  }
  else{
    pose_db_fill_depth_robust_full(n - 1, mem_start, first);
    
    n21 = (uint32_t)1 << (n - 1);
    for(j = 0; j < n21; j++){
      if(first){
        pose_db_update_label_noid(mem_start + (n21 + j) * WORD_SIZE, mem_start + j * WORD_SIZE, NULL);
      }
      else{
        pose_db_update_label_noid(mem_start + (n21 + j) * WORD_SIZE, mem_start + j * WORD_SIZE, mem_start + (n21 + j) * WORD_SIZE);
      }
    }
    
    pose_db_apply_butterfly(n - 1, mem_start + n21 * WORD_SIZE);

    pose_db_fill_depth_robust_right(n - 1, mem_start + n21 * WORD_SIZE);
    
    
  }
}


void pose_db_fill_depth_robust(uint8_t n, uint8_t *mem_start, uint8_t *challenge, uint8_t initial_cid){
  uint32_t n2, j;

  n2 = (uint32_t) 1 << n;
  memcpy(pose_db_input, challenge, WORD_SIZE);

  if(initial_cid > 0) pose_db_cid = 0;

  pose_db_fill_depth_robust_full(n, mem_start, 1);

  for(j = 0; j < n2; j++){
    pose_db_update_label_noid(mem_start + j * WORD_SIZE, mem_start + j * WORD_SIZE, NULL);
  }

  pose_db_apply_butterfly(n, mem_start);

  pose_db_fill_depth_robust_right(n, mem_start);

}



void pose_db_run(uint8_t n, uint8_t *mem_start, uint8_t *challenge){
  pose_db_fill_depth_robust(n, mem_start, challenge, 1);
}

#if (NO_COMPUTE == 0)

uint8_t pose_db_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    uint16_t q;
    *performance_mode = 0;
    if(pose_db_state == 0){
        if(*recv_addr != NULL)finished = 2;
        else{
            *recv_addr = pose_db_recv;
            *recv_length = pose_db_n_msg1;
            *send_addr = NULL;
        }
    }
    else if(pose_db_state == 1){
       if(memcmp(pose_db_recv, pose_db_msg1, pose_db_n_msg1 ) != 0)finished = 2;
       else{
           *recv_addr = pose_db_challenge;
           *recv_length = WORD_SIZE;
           *send_addr = NULL;
           *performance_mode = 1;
       }
    }
    else if(pose_db_state == 2){

        pose_db_run(MEM_SIZE_LOG, pose_db_mem, pose_db_challenge);

        *recv_addr = pose_db_recv;
        *recv_length = 2;
        memcpy(pose_db_send,pose_db_msg2, pose_db_n_msg2);
        *send_addr = pose_db_send;
        *send_length = pose_db_n_msg2;
    }
    else if(pose_db_state <= pose_db_rounds + 1){

        q = bytes2int(pose_db_recv);

        *recv_addr = pose_db_recv;
        *recv_length = 2;
        *send_addr = pose_db_mem + q * WORD_SIZE;
        *send_length = WORD_SIZE;
    }
    else if(pose_db_state == pose_db_rounds + 2){
        q = bytes2int(pose_db_recv);
        *send_addr = pose_db_mem + q * WORD_SIZE;
        *send_length = WORD_SIZE;
        *recv_addr = NULL;
        finished = 1;
    }
    pose_db_state += 1;
    if(finished != 0){
        memset(pose_db_recv, 0, WORD_SIZE);
        memset(pose_db_send, 0, WORD_SIZE);
        pose_db_state = 0;
    }
    return finished;
}


#else

uint8_t pose_db_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    *performance_mode = 0;
    if(pose_db_state == 0){
        *recv_addr = pose_db_recv;
        *recv_length = pose_db_n_msg1;
        *send_addr = NULL;
    }
    else if(pose_db_state == 1){
       *recv_addr = pose_db_challenge;
       *recv_length = WORD_SIZE;
       *send_addr = NULL;
    }
    else if(pose_db_state == 2){
        *recv_addr = pose_db_recv;
        *recv_length = 2;
        *send_addr = (uint8_t*)pose_db_msg2;
        *send_length = pose_db_n_msg2;
    }
    else if(pose_db_state <= pose_db_rounds + 1){
        *recv_addr = pose_db_recv;
        *recv_length = 2;
        *send_addr = pose_db_mem;
        *send_length = WORD_SIZE;
    }
    else if(pose_db_state == pose_db_rounds + 2){
        *send_addr = pose_db_mem;
        *send_length = WORD_SIZE;
        *recv_addr = NULL;
        finished = 1;
    }
    pose_db_state += 1;
    if(finished != 0){
        pose_db_state = 0;
    }
    return finished;
}
#endif

