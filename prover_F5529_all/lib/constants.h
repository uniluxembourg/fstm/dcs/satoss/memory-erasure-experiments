#ifndef CONSTANTS_H_
#define CONSTANTS_H_

// this must be set using -D compiler option
//#define FRAM_DEVICE
//#define FLASH_DEVICE
//#define HASH_ID 0
//#define PROTOCOL_ID 0
//#define MEM_SIZE_LOG 6
//#define NO_COMPUTE 1

#define WORD_SIZE 32
#define MEM_SIZE (1 << (MEM_SIZE_LOG))


#define INDEX_SIZE 2


#endif /* CONSTANTS_H_ */
