#include <msp430.h>
#include <stdint.h>
#include <string.h>

#include "driverlib.h"

#include "uart.h"
#include "cpuclock.h"

#include "constants.h"
#include "protohelper.h"




void run_protocol(){
    uint8_t count;
    uint8_t* recv_addr, *send_addr, recv_length, send_length;
    uint8_t performance_mode, last_performance_mode;
    uint8_t finished;
    finished = 0;
    recv_addr = NULL;
    send_addr = NULL;
    performance_mode = 0;
    while(finished == 0){
        last_performance_mode = performance_mode;
        if(performance_mode == 1){
            set_high_performace();
        }
        finished = run_protocol_prover(&recv_addr, &recv_length, &send_addr, &send_length,&performance_mode );
        if(last_performance_mode == 1){
            set_default_performace();
        }
        if(finished != 2){
            if(send_addr != NULL){
                finished = 0;
                UartSend(send_addr, send_length, 0);

            }
            if(recv_addr != NULL){
                while(1){
                    count = UartReceive(recv_addr, recv_length);
                    if(count > 0)break;
                }
                if(count != recv_length){
                    finished = 2;
                    break;
                }
            }

        }
    }
    //assert(finished == 1);
}




int main(void)
{


    WDT_A_hold(WDT_A_BASE);


#ifdef FRAM_DEVICE
    PMM_unlockLPM5();
#endif


    UartInit();

    while(1) run_protocol();


}



