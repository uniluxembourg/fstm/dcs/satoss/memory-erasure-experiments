#include "aeshash.h"

#ifdef FRAM_DEVICE
#define BLOCK_SIZE (256 / 8)
uint8_t h_i[BLOCK_SIZE];
uint8_t _tmp[BLOCK_SIZE], _tmp2[BLOCK_SIZE/2];
uint8_t i, j, jj, tmpc;

uint8_t padded_data[4 * BLOCK_SIZE];
uint8_t padded_len;

void pkcs7_pad(uint8_t* data, uint8_t data_len) {
    uint8_t padding = BLOCK_SIZE - (data_len % BLOCK_SIZE);
    uint8_t padded_size = data_len + padding;

    memcpy(padded_data, data, data_len);
    memset(padded_data + data_len, padding, padding);

    padded_len = padded_size;
}

void xor(uint8_t* a, uint8_t* b, uint8_t len){
    for(jj = 0; jj < len; jj++){
        a[jj] ^= b[jj];
    }
}


void aes128_cbc_encrypt(uint8_t *output, uint8_t *input, uint8_t* key, uint8_t len_blocks){
//    for(j = 0; j < len_blocks * BLOCK_SIZE / 2; j+= 2){
//        tmpc = input[i];
//        input[i] = input[i + 1];
//        input[i + 1] = tmpc;
//    }
    AES256_setCipherKey(AES256_BASE, key, AES256_KEYLENGTH_256BIT);
    memset(_tmp2, 0, BLOCK_SIZE/2);

    for(j = 0; j < len_blocks * BLOCK_SIZE / 2; j+= BLOCK_SIZE / 2){
        xor(_tmp2, input + j, BLOCK_SIZE / 2);
        /*for(jj = j; jj < j + BLOCK_SIZE / 2; jj += 2){
            tmpc = input[i];
            //        input[i] = input[i + 1];
            //        input[i + 1] = tmpc;
        }*/
        AES256_encryptData(AES256_BASE, _tmp2, output + j);
        memcpy(_tmp2, output + j, BLOCK_SIZE/2 );
    }
//    for(j = 0; j < len_blocks * BLOCK_SIZE / 2; j+= 2){
//        tmpc = input[i];
//        input[i] = input[i + 1];
//        input[i + 1] = tmpc;
//    }

}


void aeshash(uint8_t *output, uint8_t *input, uint8_t len){

    pkcs7_pad(input, len);

    memset(h_i, 0xff, BLOCK_SIZE);
    for(i = 0; i < padded_len; i+= BLOCK_SIZE){
        aes128_cbc_encrypt(_tmp, h_i, padded_data + i, 2);
        xor(h_i, _tmp, BLOCK_SIZE);
    }
    memcpy(_tmp, padded_data + padded_len - BLOCK_SIZE, BLOCK_SIZE);

    xor(_tmp, h_i, BLOCK_SIZE);
    aes128_cbc_encrypt(_tmp, h_i, _tmp, 2);
    xor(h_i, _tmp, BLOCK_SIZE);

    memcpy(output, h_i, BLOCK_SIZE);
}
#endif

