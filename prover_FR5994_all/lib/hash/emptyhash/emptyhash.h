#ifndef EMPTYHASH_H_
#define EMPTYHASH_H_

#include <stdint.h>
#include "constants.h"

void emptyhash(uint8_t *output, uint8_t *input, uint8_t len);


#endif /* EMPTYHASH_H_ */
