#include "emptyhash.h"


void emptyhash(uint8_t *output, uint8_t *input, uint8_t len){
    uint32_t i;
    for(i = 0; i < len; i++){
        output[i] = input[i];
    }
}
