#ifndef KARAME_H_

#define KARAME_H_

#include <string.h>
#include <stdint.h>
#include "constants.h"
#include "hashhelper.h"
#include "utils.h"

void karame_precomp(uint8_t *Ca, uint32_t n, uint8_t *k1, uint8_t *s, uint8_t *k1p);
void karame_run(uint32_t n, uint8_t *mem_start, uint8_t *challenge);

uint8_t karame_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode);

#endif /* KARAME_H_ */
