#include "karvelas.h"



uint32_t karvelas_cid;
uint8_t karvelas_tmp[WORD_SIZE], karvelas_input[3 * WORD_SIZE + 4], karvelas_source[WORD_SIZE];

uint8_t karvelas_mem[MEM_SIZE * WORD_SIZE];
const uint8_t karvelas_msg1[] = ":karvelas:\n", karvelas_n_msg1 = 11;

uint16_t karvelas_state = 0;
uint8_t karvelas_send[WORD_SIZE];
uint8_t karvelas_recv[WORD_SIZE];
uint8_t karvelas_challenge[WORD_SIZE];


void karvelas_update_label_noid(uint8_t* addr, uint8_t* pred0, uint8_t* pred1){
  uint8_t len;


  
  memcpy(karvelas_input + WORD_SIZE, &karvelas_cid, 4);
  len = WORD_SIZE + 4;
  if(pred0 != NULL){
    memcpy(karvelas_input + WORD_SIZE + 4, pred0, WORD_SIZE);
    len += WORD_SIZE;
  }
  if(pred1 != NULL){
    memcpy(karvelas_input + 2 * WORD_SIZE + 4, pred1, WORD_SIZE);
    len += WORD_SIZE;
  }

  karvelas_cid++;

  h(addr, karvelas_input, len);
}

void karvelas_update_label_with_id(uint8_t* addr, uint8_t* pred0, uint8_t* pred1, uint32_t id){
  uint8_t len;


  
  memcpy(karvelas_input + WORD_SIZE, &id, 4);
  len = WORD_SIZE + 4;
  if(pred0 != NULL){
    memcpy(karvelas_input + WORD_SIZE + 4, pred0, WORD_SIZE);
    len += WORD_SIZE;
  }
  if(pred1 != NULL){
    memcpy(karvelas_input + 2 * WORD_SIZE + 4, pred1, WORD_SIZE);
    len += WORD_SIZE;
  }

  h(addr, karvelas_input, len);
}


void karvelas_update_layer(uint32_t jump, uint32_t n2, uint8_t *mem_start){
  int32_t j, k;

  j = 0;
  while(j < n2){
      for(k = j; k < j + jump; k++){
        karvelas_update_label_noid(karvelas_tmp, mem_start + k * WORD_SIZE, mem_start + (k + jump) * WORD_SIZE);
        
        karvelas_update_label_noid(mem_start + (k + jump) * WORD_SIZE, mem_start + k * WORD_SIZE, mem_start + (k + jump) * WORD_SIZE);

        memcpy(mem_start + k * WORD_SIZE, karvelas_tmp, WORD_SIZE);

      }
      j += 2 * jump;
  }
}


void karvelas_apply_butterfly(uint8_t n, uint8_t *mem_start){
  int32_t i;
  uint32_t n2;

  n2 =  (uint32_t)1 << n;
  for(i = 0; i < n; i++){
      karvelas_update_layer((uint32_t)1 << (n - 1 - i), n2, mem_start);
  }

  for(i = 0; i < n2; i++){
    karvelas_update_label_noid(mem_start + i * WORD_SIZE, mem_start + i * WORD_SIZE, NULL);
  }

  for(i = 0; i < n; i++){
      karvelas_update_layer((uint32_t)1 << i, n2, mem_start);
  }

}

void karvelas_simple_layer(uint32_t n2, uint8_t *mem_start){
    uint32_t j;
    for(j = 0; j < n2; j++){
        karvelas_update_label_noid(mem_start + j * WORD_SIZE, mem_start + j * WORD_SIZE, NULL);
    }
}

void karvelas_ptc(uint8_t n, uint8_t *mem_start, uint8_t *tmp){
    uint32_t n2, j;
    uint8_t btmp;
    if(n == 1){
        karvelas_apply_butterfly(n, mem_start);
    }
    else{
        n2 = (uint32_t)1 << (n - 1);
        memcpy(tmp, mem_start, n2 * WORD_SIZE);

        for(j = 0; j < n2; j++){
            karvelas_update_label_noid(mem_start + j * WORD_SIZE, mem_start + j * WORD_SIZE, mem_start + (j + n2) * WORD_SIZE);
        }

        karvelas_apply_butterfly(n - 1, mem_start);

        karvelas_simple_layer(n2, mem_start);

        karvelas_ptc(n - 1, mem_start, tmp + n2 * WORD_SIZE);

        karvelas_simple_layer(n2, mem_start);

        karvelas_ptc(n - 1, mem_start, tmp + n2 * WORD_SIZE);

        karvelas_simple_layer(n2, mem_start);

        karvelas_apply_butterfly(n - 1, mem_start);

        for(j = 0; j < n2; j++){
            karvelas_update_label_noid(mem_start + (j + n2) * WORD_SIZE, mem_start + j * WORD_SIZE, mem_start + (j + n2) * WORD_SIZE);
        }

        for(j = 0; j < n2 * WORD_SIZE; j++){
            btmp = *(tmp + j);
            *(tmp + j) = *(mem_start + n2 * WORD_SIZE + j);
            *(mem_start + n2 * WORD_SIZE + j) = btmp;
        }

        for(j = 0; j < n2; j++){
            karvelas_update_label_noid(mem_start + j * WORD_SIZE, mem_start + j * WORD_SIZE, mem_start + (j + n2) * WORD_SIZE);
        }


        memcpy(mem_start + n2 * WORD_SIZE, tmp, n2 * WORD_SIZE);
    }
}

void karvelas_fill(uint8_t n, uint8_t *mem_start, uint8_t *challenge){
  uint32_t n2, n21, j;

  n2 = (uint32_t) 1 << n;
  memcpy(karvelas_input, challenge, WORD_SIZE);
  karvelas_cid = 0;
  karvelas_update_label_noid(karvelas_source, NULL, NULL);

  for(j = 0; j < n2; j++){
    karvelas_update_label_noid(mem_start + j * WORD_SIZE, karvelas_source, NULL);
  }
  n21 = (uint32_t) 1 << (n - 1);
  for(j = 0; j < n21; j++){
    karvelas_update_label_noid(mem_start + j * WORD_SIZE, mem_start + j * WORD_SIZE, mem_start + (j + n21) * WORD_SIZE);
  }
  karvelas_apply_butterfly(n - 1, mem_start);

  karvelas_simple_layer(n21, mem_start);

  karvelas_ptc(n - 1, mem_start, mem_start + n21 * WORD_SIZE);

  karvelas_simple_layer(n21, mem_start);

  karvelas_ptc(n - 1, mem_start, mem_start + n21 * WORD_SIZE);

  karvelas_simple_layer(n21, mem_start);

  karvelas_apply_butterfly(n - 1, mem_start);


  for(j = 0; j < n21; j++){
    karvelas_update_label_with_id(karvelas_tmp, karvelas_source, NULL, j + n21 + 1);
    karvelas_update_label_noid(mem_start + (j + n21) * WORD_SIZE, mem_start + j * WORD_SIZE, karvelas_tmp);
  }

  for(j = 0; j < n21; j++){
    karvelas_update_label_with_id(karvelas_tmp, karvelas_source, NULL, j + 1);
    karvelas_update_label_noid(mem_start + j * WORD_SIZE, mem_start + j * WORD_SIZE, karvelas_tmp);
  }

  for(j = 1; j < n2; j++){
    karvelas_update_label_noid(mem_start, mem_start, mem_start + j * WORD_SIZE);
  }
  

}



void karvelas_run(uint8_t n, uint8_t *mem_start, uint8_t *challenge){
  karvelas_fill(n, mem_start, challenge);
}



#if (NO_COMPUTE == 0)


uint8_t karvelas_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    *performance_mode = 0;
    if(karvelas_state == 0){
        if(*recv_addr != NULL)finished = 2;
        else{
            *recv_addr = karvelas_recv;
            *recv_length = karvelas_n_msg1;
            *send_addr = NULL;
        }
    }
    else if(karvelas_state == 1){
       if(memcmp(karvelas_recv, karvelas_msg1, karvelas_n_msg1 ) != 0)finished = 2;
       else{
           *recv_addr = karvelas_challenge;
           *recv_length = WORD_SIZE;
           *send_addr = NULL;
           *performance_mode = 1;
       }
    }
    else if(karvelas_state == 2){

        karvelas_run(MEM_SIZE_LOG, karvelas_mem, karvelas_challenge);

        *recv_addr = NULL;
        *send_addr = karvelas_mem;
        *send_length = WORD_SIZE;
        *performance_mode = 0;
        finished = 1;
    }
    karvelas_state += 1;
    if(finished != 0){
        memset(karvelas_recv, 0, WORD_SIZE);
        memset(karvelas_send, 0, WORD_SIZE);
        karvelas_state = 0;
    }
    return finished;
}

#else


uint8_t karvelas_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    *performance_mode = 0;
    if(karvelas_state == 0){
        *recv_addr = karvelas_recv;
        *recv_length = karvelas_n_msg1;
        *send_addr = NULL;
    }
    else if(karvelas_state == 1){
       *recv_addr = karvelas_challenge;
       *recv_length = WORD_SIZE;
       *send_addr = NULL;
    }
    else if(karvelas_state == 2){
        *recv_addr = NULL;
        *send_addr = karvelas_mem;
        *send_length = WORD_SIZE;
        finished = 1;
    }
    karvelas_state += 1;
    if(finished != 0){
        karvelas_state = 0;
    }
    return finished;
}


#endif


