#include "karame.h"


uint8_t S[2 * WORD_SIZE];

uint8_t karame_mem[(MEM_SIZE + 2) * WORD_SIZE];
const uint8_t karame_msg1[] = ":karame:\n", karame_n_msg1 = 9;

uint16_t karame_state = 0;
uint8_t karame_send[WORD_SIZE];
uint8_t karame_recv[WORD_SIZE];
uint8_t karame_si[WORD_SIZE + 4], karame_tmp[WORD_SIZE];
void karame_shift_XOR(uint8_t* s, uint32_t n, uint8_t* k, uint8_t* Ca, uint8_t* res){
    uint8_t l, c;
    uint32_t i, ii;


    memset(res, 0, WORD_SIZE);
    l = 5; // log2 of WORD_SIZE
    memcpy(karame_si, s, WORD_SIZE);
    
    int2bytes(0, karame_si + WORD_SIZE);
    h(S + 0 * WORD_SIZE, karame_si, WORD_SIZE + 4);
    int2bytes(1, karame_si + WORD_SIZE);
    h(S + 1 * WORD_SIZE, karame_si, WORD_SIZE + 4);
    memcpy(res, k, WORD_SIZE);
    ii = 0;
    for(i = 0; i < n; i++){

        if((i + 1) * l > (ii + 2) * WORD_SIZE * 8){
            int2bytes(ii + 2, karame_si + WORD_SIZE);
            memcpy(S, S + WORD_SIZE, WORD_SIZE);
            h(S + WORD_SIZE, karame_si, WORD_SIZE + 4);
            ii += 1;
        }
        c = bits2int(S, i * l - ii * WORD_SIZE * 8, (i + 1) * l - ii * WORD_SIZE * 8);

        bytes_cyclic_shift(Ca + i * WORD_SIZE, WORD_SIZE, c, karame_tmp);
        bytes_xor(res, karame_tmp, WORD_SIZE, res);
    }
}


void karame_precomp(uint8_t *Ca, uint32_t n, uint8_t *k1, uint8_t *s, uint8_t* k1p){
    karame_shift_XOR(s, n, k1, Ca, k1p);
}

uint8_t karame_k1p[WORD_SIZE];
uint8_t karame_s[WORD_SIZE];
uint8_t karame_k1[WORD_SIZE];
void karame_run(uint32_t n, uint8_t *mem_start, uint8_t *challenge){

    //memcpy(mem_start, challenge, n * WORD_SIZE);
    memcpy(karame_k1p, challenge + n * WORD_SIZE, WORD_SIZE);
    memcpy(karame_s, challenge + (n + 1) * WORD_SIZE, WORD_SIZE);
    karame_shift_XOR(karame_s, n, karame_k1p, mem_start, karame_k1);
    memcpy(mem_start, karame_k1, WORD_SIZE);
}



#if (NO_COMPUTE == 0)

uint8_t karame_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    *performance_mode = 0;
    if(karame_state == 0){
        if(*recv_addr != NULL)finished = 2;
        else{
            *recv_addr = karame_recv;
            *recv_length = karame_n_msg1;
            *send_addr = NULL;
        }
    }
    else if(karame_state >= 1 && karame_state <= MEM_SIZE + 2){
       if(karame_state == 1 && memcmp(karame_recv, karame_msg1, karame_n_msg1 ) != 0)finished = 2;
       else{
           *recv_addr = karame_mem + (uint32_t)(karame_state - 1) * WORD_SIZE;
           *recv_length = WORD_SIZE;
           *send_addr = NULL;
           if(karame_state == MEM_SIZE + 2){
               *performance_mode = 1;
           }
       }
    }
    else if(karame_state == MEM_SIZE + 3){

        karame_run(MEM_SIZE, karame_mem, karame_mem);

        *recv_addr = NULL;
        *send_addr = karame_mem;
        *send_length = WORD_SIZE;
        *performance_mode = 0;
        finished = 1;
    }
    karame_state += 1;
    if(finished != 0){
        memset(karame_recv, 0, WORD_SIZE);
        memset(karame_send, 0, WORD_SIZE);
        karame_state = 0;
    }
    return finished;
}


#else

uint8_t karame_run_protocol_prover(uint8_t** recv_addr, uint8_t* recv_length, uint8_t** send_addr,  uint8_t*  send_length, uint8_t* performance_mode){
    uint8_t finished = 0;
    *performance_mode = 0;
    if(karame_state == 0){
        *recv_addr = karame_recv;
        *recv_length = karame_n_msg1;
        *send_addr = NULL;
    }
    else if(karame_state >= 1 && karame_state <= MEM_SIZE + 2){
       *recv_addr = karame_mem;
       *recv_length = WORD_SIZE;
       *send_addr = NULL;
    }
    else if(karame_state == MEM_SIZE + 3){

        *recv_addr = NULL;
        *send_addr = karame_mem;
        *send_length = WORD_SIZE;
        finished = 1;
    }
    karame_state += 1;
    if(finished != 0){
        karame_state = 0;
    }
    return finished;
}

#endif
