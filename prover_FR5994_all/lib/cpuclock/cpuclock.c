#include "cpuclock.h"


#ifdef FRAM_DEVICE
void set_high_performace(){


    //CS_setExternalClockSource( LF_CRYSTAL_FREQUENCY_IN_HZ, HF_CRYSTAL_FREQUENCY_IN_HZ );

    FRAMCtl_A_configureWaitStateControl(FRAMCTL_A_ACCESS_TIME_CYCLES_1);

    // Set DCO to run at 1MHz
    CS_setDCOFreq( CS_DCORSEL_0, CS_DCOFSEL_0 );



    // Set ACLK to use VLOCLK as its oscillator source (10KHz)
    CS_initClockSignal( CS_ACLK, CS_VLOCLK_SELECT, CS_CLOCK_DIVIDER_4 );

    // Set SMCLK to use DCO as its oscillator source
    CS_initClockSignal( CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_4 );

    // Set MCLK to use DCO as its oscillator source
    CS_initClockSignal( CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_4 );

    CS_setDCOFreq( CS_DCORSEL_1, CS_DCOFSEL_4 );

    __delay_cycles(60);


    // Set ACLK to use VLOCLK as its oscillator source (10KHz)
    CS_initClockSignal( CS_ACLK, CS_VLOCLK_SELECT, CS_CLOCK_DIVIDER_1 );

    // Set SMCLK to use DCO as its oscillator source
    CS_initClockSignal( CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1 );

    // Set MCLK to use DCO as its oscillator source
    CS_initClockSignal( CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1 );


}
void set_default_performace(){
    CS_setDCOFreq( CS_DCORSEL_0, CS_DCOFSEL_0 );

    CS_initClockSignal(CS_ACLK,CS_DCOCLK_SELECT,CS_CLOCK_DIVIDER_1);


    CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);

    FRAMCtl_A_configureWaitStateControl(FRAMCTL_A_ACCESS_TIME_CYCLES_0);

}

#endif

#ifdef FLASH_DEVICE

#define REFO_FREQ         32768
#define XT1_FREQ          32768
#define XT2_FREQ          4000000
#define MCLK_FREQ_HIGH    25000000
#define MCLK_FREQ_DEF     1048576

#define XT1_KHZ           (XT1_FREQ / 1000)
#define XT2_KHZ           (XT2_FREQ / 1000)
#define MCLK_KHZ_HIGH     (MCLK_FREQ_HIGH / 1000)
#define MCLK_KHZ_DEF      (MCLK_FREQ_DEF / 1000)

#define scale_factor       4

#define MCLK_FLLREF_RATIO_HIGH (MCLK_KHZ_HIGH / (XT2_KHZ / scale_factor))
#define MCLK_FLLREF_RATIO_DEF (MCLK_FREQ_DEF / REFO_FREQ)



void set_high_performace(){
    PMM_setVCore(PMM_CORE_LEVEL_3);

    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P5,
                                               (GPIO_PIN4 | GPIO_PIN2));

    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P5,
                                                (GPIO_PIN5 | GPIO_PIN3));

    UCS_setExternalClockSource(XT1_FREQ,
                               XT2_FREQ);

    UCS_turnOnXT2(UCS_XT2_DRIVE_4MHZ_8MHZ);

    UCS_turnOnLFXT1(UCS_XT1_DRIVE_0,
                    UCS_XCAP_3);

    UCS_initClockSignal(UCS_FLLREF,
                        UCS_XT2CLK_SELECT,
                        UCS_CLOCK_DIVIDER_4);

    UCS_initFLLSettle(MCLK_KHZ_HIGH,
                      MCLK_FLLREF_RATIO_HIGH);

    UCS_initClockSignal(UCS_SMCLK,
                        UCS_REFOCLK_SELECT,
                        UCS_CLOCK_DIVIDER_1);

    UCS_initClockSignal(UCS_ACLK,
                        UCS_XT1CLK_SELECT,
                        UCS_CLOCK_DIVIDER_1);
}
void set_default_performace(){

    UCS_turnOffXT1();
    UCS_turnOffXT2();


    UCS_initClockSignal(UCS_FLLREF,UCS_REFOCLK_SELECT, UCS_CLOCK_DIVIDER_1);
    UCS_initClockSignal(UCS_ACLK,UCS_REFOCLK_SELECT,UCS_CLOCK_DIVIDER_1);

    UCS_initFLLSettle(MCLK_KHZ_DEF,MCLK_FLLREF_RATIO_DEF);

    UCS_initClockSignal(UCS_SMCLK,UCS_DCOCLKDIV_SELECT,UCS_CLOCK_DIVIDER_1);

    PMM_setVCore(PMM_CORE_LEVEL_0);
}
#endif

