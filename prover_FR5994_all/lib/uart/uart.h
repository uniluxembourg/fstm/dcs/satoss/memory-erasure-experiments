#ifndef UART_H_
#define UART_H_

#include <stdint.h>
#include <msp430.h>
#include "driverlib.h"
#include "constants.h"

#define RXBUF_SIZE (128)



void UartInit(void);
void UartSend(uint8_t* buf, uint8_t len, uint8_t end_line);
uint8_t UartReceive(uint8_t* buf, uint8_t n);

#endif /* UART_H_ */
