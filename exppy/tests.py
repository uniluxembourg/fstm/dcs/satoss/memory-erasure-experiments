# /usr/bin/env python3 -u

import random
from timeit import timeit
from hashlib import sha256
from lib.constants import WORD_SIZE
from lib.proto import (
    pose_db,
    karvelas,
    dziembowski,
    perito,
    karame,
    pose_db_light,
    pose_db_random,
)


if __name__ == "__main__":

    MEM_SIZE_LOG = 6  # 6 for the assertions below
    m = 2**MEM_SIZE_LOG
    REPS = 10

    random.seed(0)

    ASSERTIONS = True

    challenge = random.randbytes(WORD_SIZE)
    # print(challenge.hex())
    big_challenge = random.randbytes(WORD_SIZE * m)
    # print(big_challenge.hex())

    p0 = pose_db.pose_db(m, sha256)
    tt = timeit(lambda: p0.run(challenge), number=REPS) / REPS
    res = p0.mem[-1].hex()
    # print(''.join(c.hex() for c in p0.mem))
    print(p0.__class__.__name__, tt)
    pose_db_result = "e903a3609110a9fcb5673bf27e7b85caa5c0af4627bbabf487182edecf2465d5"
    if ASSERTIONS:
        assert res == pose_db_result

    p2 = karvelas.karvelas(m, sha256)
    tt = timeit(lambda: p2.run(challenge).hex(), number=REPS) / REPS
    res = p2.run(challenge).hex()
    print(p2.__class__.__name__, tt)
    karvelas_result = "694aa2650a0dc6229f5751c65fcc78064e5d43b87b0e197126e60eb0ee2c5818"
    if ASSERTIONS:
        assert res == karvelas_result

    p3 = dziembowski.dziembowski(m, sha256)
    tt = timeit(lambda: p3.run(challenge).hex(), number=REPS) / REPS
    res = p3.run(challenge).hex()
    print(p3.__class__.__name__, tt)
    dziembowski_result = (
        "ebe18950f527582573956d93b70a82d033eeddc5dccb137e80d0c730a12478a1"
    )
    if ASSERTIONS:
        assert res == dziembowski_result

    p4 = perito.perito(m, sha256)
    tt = timeit(lambda: p4.run(big_challenge).hex(), number=REPS) / REPS
    res = p4.run(big_challenge).hex()
    print(p4.__class__.__name__, tt)
    perito_result = "c482b1d024459f855959e2e79de186f28002d8c89c0b5f4e9f92b07860a264ba"
    if ASSERTIONS:
        assert res == perito_result

    p5 = karame.karame(m, sha256)
    s = random.randbytes(WORD_SIZE)
    # print(s.hex())
    k1p = p5.precomp(big_challenge, challenge, s)
    # print(k1p.hex())
    tt = timeit(lambda: p5.run(big_challenge + k1p + s).hex(), number=REPS) / REPS
    res = p5.run(big_challenge + k1p + s).hex()
    print(p5.__class__.__name__, tt)
    karame_result = "cd072cd8be6f9f62ac4c09c28206e7e35594aa6b342f5d0a3a5e4842fab428f7"
    if ASSERTIONS:
        assert res == karame_result

    p6 = pose_db_light.pose_db_light(m, sha256)
    tt = timeit(lambda: p6.run(challenge), number=REPS) / REPS
    res = p6.mem[-1].hex()
    # print(''.join(c.hex() for c in p7.mem))
    print(p6.__class__.__name__, tt)
    pose_db_light_result = (
        "c83e239f7cbf99053f18ae4bd4c98f97908bd4644b8a05c5ae83f52eec3a6271"
    )
    if ASSERTIONS:
        assert res == pose_db_light_result

    p7 = pose_db_random.pose_db_random(m, sha256)
    tt = timeit(lambda: p7.run(big_challenge), number=REPS) / REPS
    res = p7.mem[-1].hex()
    # print(''.join(c.hex() for c in p7.mem))
    print(p7.__class__.__name__, tt)
    pose_db_random_result = (
        "a94cc7bfce6411f6fb058ab3a1718b9b87cbc5a7f39c967e26125db6c995e6a4"
    )
    if ASSERTIONS:
        assert res == pose_db_random_result
