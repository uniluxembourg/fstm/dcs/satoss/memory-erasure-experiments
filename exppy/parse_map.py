#! /usr/bin/env python3
import sys
import json

if __name__ == "__main__":
    if len(sys.argv) < 2:
        exit(1)
    mapfile = sys.argv[1]
    with open(mapfile) as ff:

        started = False
        ll = []
        for line in ff.readlines():
            if "MODULE SUMMARY" in line:
                started = True
            if "LINKER GENERATED COPY TABLES" in line:
                break
            if started:
                ll.append(line.strip())
        assert started
    data = {}
    module = False
    modhash = False
    lastl = ""
    for l in ll:
        if "Module" in l:
            module = True
        if "/home/" in l and module:
            module = False
        if module:
            if "lib/hash" in l and l != "./lib/hash/":
                modhash = True
                name = l.split("/")[-2] + ".obj"
            if modhash:
                if "Total:" in l:
                    l = l.split()
                    val = sum(map(int, l[1:]))
                    assert name not in data
                    data[name] = val
                    modhash = False
            elif ".obj" in l:
                l = l.split()
                name = l[0]
                val = sum(map(int, l[1:]))
                assert name not in data
                data[name] = val
        if "Grand Total:" in l:
            data["total"] = sum(map(int, l.split()[-3:]))
            data["rwdata"] = int(l.split()[-1])

    print(json.dumps(data))
