#! /usr/bin/env python3

from .constants import WORD_SIZE


def dummy_h(a):
    output = list(a[-WORD_SIZE:])
    for i in range(WORD_SIZE):
        output[i] = (output[i] + 1) % (1 << 8)
    return bytes(output)


def int2bytes(a, n=4):
    return a.to_bytes(n, "little")


def bytes_xor(a, b):
    return b"".join([(x ^ y).to_bytes(1, "little") for x, y in zip(a, b)])


def bytes_cyclic_shift(a, n):
    ab = bin(int.from_bytes(a, "little"))[2:]
    ab = "0" * (len(a) * 8 - len(ab)) + ab
    ab = ab[-n:] + ab[:-n]
    return int(ab, 2).to_bytes(len(a), "little")


def bytes2bits(a):
    bit8pad = lambda x: ("0" * (8 - len(x)) + x)[::-1]
    res = "".join([bit8pad(bin(b)[2:]) for b in a])
    return res


def bits2int(a):
    return int(a, 2)
