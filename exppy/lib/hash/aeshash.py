from Crypto.Cipher import AES
from Crypto.Util.Padding import pad
from Crypto.Util.strxor import strxor


class aeshash:
    def __init__(self, a: bytes):
        a_padded = pad(a, 32)

        h_i = bytes([0xFF] * 32)
        for i in range(0, len(a_padded), 32):
            x_i = a_padded[i : i + 32]
            cipher = AES.new(x_i, AES.MODE_CBC, iv=bytes(16))
            h_i = strxor(cipher.encrypt(h_i), h_i)
        cipher = AES.new(strxor(a_padded[-32:], h_i), AES.MODE_CBC, iv=bytes(16))
        self.h_i = strxor(cipher.encrypt(h_i), h_i)

    def digest(self):
        return self.h_i


if __name__ == "__main__":
    assert (
        aeshash(b"\0").digest().hex()
        == "a18f009cfdfff504072bd395bed1389cff6a040983f9a24c28717566345b60e9"
    )
