from ascon import hash


class ascon:
    def __init__(self, a: bytes):
        self.h_i = hash(a, variant="Ascon-Hasha")

    def digest(self):
        return self.h_i


if __name__ == "__main__":
    assert (
        ascon(b"\0").digest().hex()
        == "5a55f0367763d334a3174f9c17fa476eb9196a22f10daf29505633572e7756e4"
    )
