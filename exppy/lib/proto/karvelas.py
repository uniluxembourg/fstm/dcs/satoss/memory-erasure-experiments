#! /usr/bin/env python3
"""
Implementation for paper: 'Efficient Proofs of Secure Erasure'
https://doi.org/10.1007/978-3-319-10879-7_30
"""

from math import log2
from random import randbytes

from ..constants import *
from ..utils import *


class karvelas:

    def __init__(self, m, h):
        n = int(log2(m))
        assert 2**n == m
        self.m = m
        self.n = n
        self.mem = [None] * m
        self.h = h
        self.state = 0

    def run_empty_protocol(self):
        if self.state == 0:
            msg = b":karvelas:\n"
            self.state += 1
            return msg, None
        elif self.state == 1:
            msg = b"\0" * WORD_SIZE
            self.state += 1
            return msg, WORD_SIZE
        elif self.state == 2:
            return True
        return False

    def run_protocol_verifier(self, lastmsg, NO_COMPUTE):
        if NO_COMPUTE:
            return self.run_empty_protocol()
        if self.state == 0:
            if lastmsg is not None:
                return False
            msg = b":karvelas:\n"
            self.state += 1
            return msg, None
        elif self.state == 1:
            if lastmsg is not None:
                return False
            challenge = randbytes(WORD_SIZE)
            msg = challenge
            self.challenge = challenge
            self.state += 1
            return msg, WORD_SIZE
        elif self.state == 2:
            if type(lastmsg) != bytes or len(lastmsg) != WORD_SIZE:
                return False
            res = self.run(self.challenge)
            if res != lastmsg:
                return False
            return True
        return False

    def parse_timings(self, tt):
        data = {}
        data["compute_time"] = tt[0][1] - tt[0][0]
        data["total_time"] = tt[-1][1] - tt[-1][0]
        return data

    def run(self, challenge):
        self.cid = 0
        self.prefix_challenge = challenge
        self._fill()
        return self.mem[0]

    def _update_label_noid(self, pred0, pred1):
        res = self.h(
            self.prefix_challenge + int2bytes(self.cid) + pred0 + pred1
        ).digest()
        self.cid += 1
        return res

    def _update_label_id(self, p, p1, p2):
        l1 = self.mem[p1] if p1 is not None else b""
        l2 = self.mem[p2] if p2 is not None else b""
        self.mem[p] = self._update_label_noid(l1, l2)

    def _update_label_cross_id(self, p1, p2):
        tmp = self._update_label_noid(self.mem[p1], self.mem[p2])
        self.mem[p2] = self._update_label_noid(self.mem[p1], self.mem[p2])
        self.mem[p1] = tmp

    def _apply_butterfly(self, offset, n):
        n2 = 2**n

        def update_layer(jump):
            j = 0
            while j < n2:
                for k in range(j, j + jump):
                    id1, id2 = k, k + jump
                    self._update_label_cross_id(id1 + offset, id2 + offset)
                j += 2 * jump

        for i in range(n):
            update_layer(2 ** (n - 1 - i))

        for j in range(n2):
            self._update_label_id(j + offset, j + offset, None)

        for i in range(n):
            update_layer(2**i)

    def _simple_layer(self, offset, n):
        for j in range(2**n):
            self._update_label_id(j + offset, j + offset, None)

    def _ptc(self, offset, n):
        if n == 1:
            self._apply_butterfly(offset, n)
        else:
            n2 = 2 ** (n - 1)

            tmp = self.mem[offset : offset + n2]

            for j in range(n2):
                self._update_label_id(j + offset, j + offset, j + n2 + offset)

            self._apply_butterfly(offset, n - 1)

            self._simple_layer(offset, n - 1)

            self._ptc(offset, n - 1)

            self._simple_layer(offset, n - 1)

            self._ptc(offset, n - 1)

            self._simple_layer(offset, n - 1)

            self._apply_butterfly(offset, n - 1)

            for j in range(n2):
                self._update_label_id(j + n2 + offset, j + offset, j + n2 + offset)

            tmp, self.mem[offset + n2 : offset + 2 * n2] = (
                self.mem[offset + n2 : offset + 2 * n2],
                tmp,
            )

            for j in range(n2):
                self._update_label_id(j + offset, j + offset, j + n2 + offset)

            self.mem[offset + n2 : offset + 2 * n2] = tmp

    def _fill(self):

        offset = 0

        n2 = 2**self.n

        # pebble the unique source
        self._update_label_id(offset + n2 - 1, None, None)

        for j in range(n2):
            self._update_label_id(offset + j, offset + n2 - 1, None)

        self._ptc(offset, self.n)

        # pebble the unique sink

        for j in range(1, n2):
            self._update_label_id(offset, offset, offset + j)
