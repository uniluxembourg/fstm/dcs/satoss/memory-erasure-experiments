#! /usr/bin/env python3
"""
Implementation for paper: 'Secure code update for embedded devices via proofs of secure erasure'
https://doi.org/10.1007/978-3-642-15497-3_39
"""

from random import randbytes
import hmac
from hashlib import sha256

from ..constants import *
from ..utils import *


class perito:

    def __init__(self, m, h, k=WORD_SIZE):
        self.m = m
        self.k = k
        assert k < m * WORD_SIZE
        self.h = h
        self.state = 0

    def run_empty_protocol(self):
        if self.state == 0:
            msg = b":perito:\n"
            self.state += 1
            return msg, None
        elif 1 <= self.state < self.m + 1:
            msg = b"\0" * WORD_SIZE
            self.state += 1
            return msg, WORD_SIZE if self.state == self.m + 1 else None
        elif self.state == self.m + 1:
            return True
        return False

    def run_protocol_verifier(self, lastmsg, NO_COMPUTE):
        if NO_COMPUTE:
            return self.run_empty_protocol()
        if self.state == 0:
            if lastmsg is not None:
                return False
            msg = b":perito:\n"
            self.state += 1
            return msg, None
        elif 1 <= self.state < self.m + 1:
            if lastmsg is not None:
                return False
            if self.state == 1:
                self.challenge = randbytes(self.m * WORD_SIZE)
            msg = self.challenge[(self.state - 1) * WORD_SIZE : self.state * WORD_SIZE]
            self.state += 1
            return msg, WORD_SIZE if self.state == self.m + 1 else None
        elif self.state == self.m + 1:
            if type(lastmsg) != bytes or len(lastmsg) != WORD_SIZE:
                return False
            res = self.run(self.challenge)
            # print('res', res)
            if res != lastmsg:
                return False
            return True
        return False

    def parse_timings(self, tt):
        data = {}
        data["compute_time"] = tt[0][1] - tt[0][0]
        data["total_time"] = tt[-1][1] - tt[-1][0]
        return data

    def run(self, challenge):
        assert len(challenge) == self.m * WORD_SIZE
        data = challenge[: -self.k]
        key = challenge[-self.k :]
        res = hmac.digest(key, data, sha256)
        return res
