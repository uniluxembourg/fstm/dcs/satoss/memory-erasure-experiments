#! /usr/bin/env python3

from random import randbytes, randint

from ..constants import *
from ..utils import *


class pose_db_random:

    def __init__(self, m, h):
        self.m = m
        self.mem = [None] * m
        self.h = h
        self.rounds = 71
        self.state = 0

    def run_empty_protocol(self):
        if self.state == 0:
            msg = b":pose_db_random:\n"
            self.state += 1
            return msg, None
        elif 1 <= self.state < self.m + 1:
            msg = b"\0" * WORD_SIZE
            self.state += 1
            return msg, 0 if self.state == self.m + 1 else None
        elif self.state == self.m + 1:
            self.state += 1
            msg = b"\0\0"
            return msg, WORD_SIZE
        elif self.m + 2 <= self.state <= self.m + self.rounds:
            self.state += 1
            msg = b"\0\0"
            return msg, WORD_SIZE
        elif self.state == self.m + self.rounds + 1:
            return True
        return False

    def run_protocol_verifier(self, lastmsg, NO_COMPUTE):
        if NO_COMPUTE:
            return self.run_empty_protocol()
        if self.state == 0:
            if lastmsg is not None:
                return False
            msg = b":pose_db_random:\n"
            self.state += 1
            return msg, None
        elif 1 <= self.state < self.m + 1:
            if lastmsg is not None:
                return False
            if self.state == 1:
                self.challenge = randbytes(self.m * WORD_SIZE)
            msg = self.challenge[(self.state - 1) * WORD_SIZE : self.state * WORD_SIZE]
            self.mem[self.state - 1] = msg
            self.state += 1
            return msg, 0 if self.state == self.m + 1 else None
        elif self.state == self.m + 1:
            if lastmsg != b":ready:\n":
                return False
            self.state += 1
            self.q = randint(0, self.m - 1)
            msg = int2bytes(self.q, 2)
            return msg, WORD_SIZE
        elif self.m + 2 <= self.state <= self.m + self.rounds:
            if type(lastmsg) != bytes or len(lastmsg) != WORD_SIZE:
                return False
            if self.mem[self.q] != lastmsg:
                return False
            # print(self.state - 1)
            self.state += 1
            self.q = randint(0, self.m - 1)
            msg = int2bytes(self.q, 2)
            return msg, WORD_SIZE
        elif self.state == self.m + self.rounds + 1:
            if type(lastmsg) != bytes or len(lastmsg) != WORD_SIZE:
                return False
            if self.mem[self.q] != lastmsg:
                return False
            return True
        return False

    def parse_timings(self, tt):
        data = {}
        data["total_time"] = tt[-1][1] - tt[-1][0]
        data["query_time"] = [t2 - t1 for t1, t2 in tt[-1 - self.rounds : -1]]
        return data

    def run(self, challenge):
        for i in range(self.m):
            self.mem[i] = challenge[i * WORD_SIZE : (i + 1) * WORD_SIZE]
