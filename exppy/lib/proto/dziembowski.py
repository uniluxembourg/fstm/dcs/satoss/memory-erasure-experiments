#! /usr/bin/env python3
"""
Implementation for paper: 'One-time computable self-erasing functions'
https://doi.org/10.1007/978-3-642-19571-6_9
"""

from math import log2
from random import randbytes

from ..constants import *
from ..utils import *


class dziembowski:

    def __init__(self, m, h):
        n = int(log2(m))
        assert 2**n == m
        self.m = m
        self.n = n
        self.mem = [None] * m
        self.h = h
        self.state = 0

    def run_empty_protocol(self):
        if self.state == 0:
            msg = b":dziembowski:\n"
            self.state += 1
            return msg, None
        elif self.state == 1:
            msg = b"\0" * WORD_SIZE
            self.state += 1
            return msg, WORD_SIZE
        elif self.state == 2:
            return True
        return False

    def run_protocol_verifier(self, lastmsg, NO_COMPUTE=False):
        if NO_COMPUTE:
            return self.run_empty_protocol()
        if self.state == 0:
            if lastmsg is not None:
                return False
            msg = b":dziembowski:\n"
            self.state += 1
            return msg, None
        elif self.state == 1:
            if lastmsg is not None:
                return False
            challenge = randbytes(WORD_SIZE)
            msg = challenge
            self.challenge = challenge
            self.state += 1
            return msg, WORD_SIZE
        elif self.state == 2:
            if type(lastmsg) != bytes or len(lastmsg) != WORD_SIZE:
                return False
            res = self.run(self.challenge)
            if res != lastmsg:
                return False
            return True
        return False

    def parse_timings(self, tt):
        data = {}
        data["compute_time"] = tt[0][1] - tt[0][0]
        data["total_time"] = tt[-1][1] - tt[-1][0]
        return data

    def run(self, challenge):
        self.cid = 0
        self.prefix_challenge = challenge
        self._fill()
        return self.mem[0]

    def _update_label_noid(self, pred0, pred1):
        res = self.h(
            self.prefix_challenge + int2bytes(self.cid) + pred0 + pred1
        ).digest()
        self.cid += 1
        return res

    def _update_label_id(self, p, p1, p2):
        l1 = self.mem[p1] if p1 is not None else b""
        l2 = self.mem[p2] if p2 is not None else b""
        self.mem[p] = self._update_label_noid(l1, l2)

    def _apply_layer(self, offset, n):
        for i in range(offset, offset + n - 1):
            self._update_label_id(i, i, i + 1)

    def _fill(self):

        offset = 0
        n2 = 2**self.n
        for j in range(n2):
            self._update_label_id(offset + j, None, None)

        for j in range(n2 - 1):
            self._apply_layer(offset, n2 - j)
