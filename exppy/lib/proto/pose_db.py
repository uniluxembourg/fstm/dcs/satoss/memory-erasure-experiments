#! /usr/bin/env python3
"""
Implementation for paper submitted to CSF'24
"""
from math import log2
from random import randint, randbytes
from ..constants import *
from ..utils import *


class pose_db:

    def __init__(self, m, h):
        self.cid = None
        self.preffix_challenge = None
        self.h = h
        self.mem = [None] * m
        self.m = m
        self.rounds = 71
        self.state = 0

    def run_empty_protocol(self):
        if self.state == 0:
            msg = b":pose_db:\n"
            self.state += 1
            return msg, None
        elif self.state == 1:
            msg = b"\0" * WORD_SIZE
            self.state += 1
            return msg, 0
        elif self.state == 2:
            self.state += 1
            msg = b"\0\0"
            return msg, WORD_SIZE
        elif 2 <= self.state <= self.rounds + 1:
            self.state += 1
            msg = b"\0\0"
            return msg, WORD_SIZE
        elif self.state == self.rounds + 2:
            return True
        return False

    def run_protocol_verifier(self, lastmsg, NO_COMPUTE):
        if NO_COMPUTE:
            return self.run_empty_protocol()
        if self.state == 0:
            if lastmsg is not None:
                return False
            msg = b":pose_db:\n"
            self.state += 1
            return msg, None
        elif self.state == 1:
            if lastmsg is not None:
                return False
            challenge = randbytes(WORD_SIZE)
            msg = challenge
            self.challenge = challenge
            self.state += 1
            return msg, 0
        elif self.state == 2:
            if lastmsg != b":ready:\n":
                return False
            self.run(self.challenge)
            self.state += 1
            self.q = randint(0, self.m - 1)
            msg = int2bytes(self.q, 2)
            return msg, WORD_SIZE
        elif 2 <= self.state <= self.rounds + 1:
            if type(lastmsg) != bytes or len(lastmsg) != WORD_SIZE:
                return False
            if self.mem[self.q] != lastmsg:
                return False
            # print(self.state - 1)
            self.state += 1
            self.q = randint(0, self.m - 1)
            msg = int2bytes(self.q, 2)
            return msg, WORD_SIZE
        elif self.state == self.rounds + 2:
            if type(lastmsg) != bytes or len(lastmsg) != WORD_SIZE:
                return False
            if self.mem[self.q] != lastmsg:
                return False
            return True
        return False

    def parse_timings(self, tt):
        data = {}
        data["compute_time"] = tt[0][1] - tt[0][0]
        data["total_time"] = tt[-1][1] - tt[-1][0]
        data["query_time"] = [t2 - t1 for t1, t2 in tt[1:-1]]
        return data

    def run(self, challenge):
        n = int(log2(self.m))
        assert 2**n == self.m
        self.cid = 0
        self.preffix_challenge = challenge
        self._fill(n, 0)

    def _update_label_noid(self, pred0, pred1):
        res = self.h(
            self.preffix_challenge + int2bytes(self.cid) + pred0 + pred1
        ).digest()
        self.cid += 1
        return res

    def _update_label_id(self, p, p1, p2):
        l1 = self.mem[p1] if p1 is not None else b""
        l2 = self.mem[p2] if p2 is not None else b""
        self.mem[p] = self._update_label_noid(l1, l2)

    def _update_label_cross_id(self, p1, p2):
        tmp = self._update_label_noid(self.mem[p1], self.mem[p2])
        self.mem[p2] = self._update_label_noid(self.mem[p1], self.mem[p2])
        self.mem[p1] = tmp

    def _apply_butterfly(self, offset, n):
        n2 = 2**n

        def update_layer(jump):
            j = 0
            while j < n2:
                for k in range(j, j + jump):
                    id1, id2 = k, k + jump
                    self._update_label_cross_id(id1 + offset, id2 + offset)
                j += 2 * jump

        for i in range(n):
            update_layer(2 ** (n - 1 - i))

        for j in range(n2):
            self._update_label_id(j + offset, j + offset, None)

        for i in range(n):
            update_layer(2**i)

    def _fill_right(self, offset, n):
        if n == 0:
            self._update_label_id(offset, offset, None)
        else:

            for j in range(1, 2**n):
                self._update_label_id(offset + j, offset + j, None)

            jj = 1
            for j in range(n):
                self._apply_butterfly(offset + jj, j)
                jj += 2**j

            self._fill_full(offset, n - 1, False)

            n21 = 2 ** (n - 1)
            for j in range(n21):
                self._update_label_id(offset + n21 + j, offset + j, offset + n21 + j)

            self._apply_butterfly(offset + n21, n - 1)

            self._fill_right(offset + n21, n - 1)

    def _fill_full(self, offset, n, first):
        if n == 0:
            if first:
                self._update_label_id(offset, None, None)
            else:
                self._update_label_id(offset, offset, None)
        else:
            self._fill_full(offset, n - 1, first)

            n21 = 2 ** (n - 1)
            for j in range(n21):
                if first:
                    self._update_label_id(offset + n21 + j, offset + j, None)
                else:
                    self._update_label_id(
                        offset + n21 + j, offset + j, offset + n21 + j
                    )

            self._apply_butterfly(offset + 2 ** (n - 1), n - 1)

            self._fill_right(offset + 2 ** (n - 1), n - 1)

    def _fill(self, n, offset):

        self._fill_full(offset, n, True)

        for j in range(2**n):
            self._update_label_id(offset + j, offset + j, None)

        self._apply_butterfly(offset, n)

        self._fill_right(offset, n)
