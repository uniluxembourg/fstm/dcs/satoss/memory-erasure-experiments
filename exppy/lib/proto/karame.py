#! /usr/bin/env python3
"""
Implementation for paper: 'Secure Erasure and Code Update in Legacy Sensors'
https://doi.org/10.1007/978-3-319-22846-4_17
"""
from math import log2, ceil
from random import randbytes
from ..constants import *
from ..utils import *


class karame:

    def __init__(self, m, h, k=WORD_SIZE):
        self.m = m
        self.k = k
        self.h = h
        assert k < m * WORD_SIZE
        self.state = 0

    def run_empty_protocol(self):
        if self.state == 0:
            msg = b":karame:\n"
            self.state += 1
            return msg, None
        elif 1 <= self.state <= self.m + 2:
            msg = b"\0" * WORD_SIZE
            self.state += 1
            return msg, WORD_SIZE if self.state == self.m + 3 else None
        elif self.state == self.m + 3:
            return True
        return False

    def run_protocol_verifier(self, lastmsg, NO_COMPUTE):
        if NO_COMPUTE:
            return self.run_empty_protocol()
        if self.state == 0:
            if lastmsg is not None:
                return False
            msg = b":karame:\n"
            self.state += 1
            return msg, None
        elif 1 <= self.state <= self.m + 2:
            if lastmsg is not None:
                return False
            if self.state == 1:
                self.challenge = randbytes(self.m * WORD_SIZE)
                self.k1 = randbytes(WORD_SIZE)
                self.s = randbytes(WORD_SIZE)
                self.k1p = self.precomp(self.challenge, self.k1, self.s)
            if self.state <= self.m:
                msg = self.challenge[
                    (self.state - 1) * WORD_SIZE : self.state * WORD_SIZE
                ]
            elif self.state == self.m + 1:
                msg = self.k1p
            else:
                msg = self.s
            self.state += 1
            return msg, WORD_SIZE if self.state == self.m + 3 else None
        elif self.state == self.m + 3:
            if type(lastmsg) != bytes or len(lastmsg) != WORD_SIZE:
                return False
            if lastmsg != self.k1:
                return False
            return True
        return False

    def parse_timings(self, tt):
        data = {}
        data["compute_time"] = tt[0][1] - tt[0][0]
        data["total_time"] = tt[-1][1] - tt[-1][0]
        return data

    def _shiftXOR(self, s, k1, C):
        k1p = b"\0" * self.k
        l = int(log2(self.k))
        S = "".join(
            [
                bytes2bits(self.h(s + int2bytes(i)).digest())
                for i in range(ceil(self.m * l / self.k))
            ]
        )[: self.m * l]
        k1p = k1
        for i in range(self.m):
            c = bits2int(S[i * l : (i + 1) * l])
            tmp = bytes_cyclic_shift(C[i * self.k : (i + 1) * self.k], c)
            k1p = bytes_xor(k1p, tmp)
        return k1p

    def precomp(self, C, k1, s):
        return self._shiftXOR(s, k1, C)

    def run(self, challenge):
        assert len(challenge) == (self.m + 2) * WORD_SIZE
        C = challenge[: -2 * self.k]
        k1p = challenge[-2 * self.k : -self.k]
        s = challenge[-self.k :]
        res = self._shiftXOR(s, k1p, C)
        return res
