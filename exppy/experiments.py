#! /usr/bin/env python3 -u
import subprocess
import os
import json
import time


devices = ["F5529", "FR5994", "CC2652"]
attest_size = [[6], [6], [6, 7, 8]]
hashes = [
    "sha256",
    "blake2",
    "blake3",
    "ascon",
    "aeshash",
    "sha256hw",
    "emptyhash",
]
supported = {
    "F5529": list(range(4)) + [6],
    "FR5994": list(range(5)) + [6],
    "CC2652": list(range(7)),
}
algos = [
    "pose_db",
    "dziembowski",
    "baseline",
    "karvelas",
    "karame",
    "perito",
    "pose_db_light",
    "pose_db_random",
]
root = "~/ti/workspace_v12"


def cd(d):
    os.chdir(os.path.expanduser(d))


def exec_script(
    cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, verbose=True
):
    if verbose:
        print(cmd)
    sb = subprocess.run(cmd, shell=True, text=True, stderr=stderr, stdout=stdout)
    return sb


def parse_map(dn):
    d = exec_script("pwd", stdout=subprocess.PIPE, verbose=False).stdout.strip()
    cd(root + "/exppy/")
    res = exec_script(
        "python parse_map.py %s/prover_%s_all.map" % (d, devices[dn]),
        stdout=subprocess.PIPE,
    )

    cd(d)
    if res.returncode == 0:
        return res.stdout.strip()
    else:
        return None


def run_verifier(an, hn, dn, asn, NO_COMPUTE):
    d = exec_script("pwd", stdout=subprocess.PIPE, verbose=False).stdout.strip()
    cd(root + "/exppy/")
    exec_script(
        'sed -i "s/ATTEST_SIZE_LOG = [0-9]/ATTEST_SIZE_LOG = %d/" \
         verifier_all.py'
        % asn
    )
    exec_script('sed -i -E "s/DEBUG = (True|False)/DEBUG = False/" verifier_all.py')
    exec_script('sed -i "s/DEVICE_ID = [0-9]/DEVICE_ID = %d/" verifier_all.py' % dn)
    exec_script('sed -i "s/HASH_ID = [0-9]/HASH_ID = %d/" verifier_all.py' % hn)
    exec_script(
        'sed -i -E "s/NO_COMPUTE = (True|False)/NO_COMPUTE = %s/" verifier_all.py'
        % str(NO_COMPUTE)
    )
    exec_script(
        'sed -i "s/PROTOCOL_ID = [0-9]/PROTOCOL_ID = %d/" \
         verifier_all.py'
        % an
    )
    res = exec_script("python verifier_all.py", stdout=subprocess.PIPE)

    cd(d)
    if res.returncode == 0:
        return res.stdout.strip()
    else:
        return None


def run_experiment(dn, asn, an, hn, NO_COMPUTE, DEBUG=False):
    if not DEBUG:
        outfile = "exppy/results/%s-%s-%s-%s-%s.json" % (
            hashes[hn],
            algos[an],
            devices[dn],
            asn,
            "no_compute" if NO_COMPUTE else "compute",
        )
        if os.path.exists(outfile):
            print(outfile, "exists")
            # print(devices[dn], algos[an], hashes[hn])
            return
    print(
        devices[dn],
        f"{2 ** (asn - 5)}KB",
        algos[an],
        hashes[hn],
        "no_compute" if NO_COMPUTE else "compute",
    )
    cd("prover_" + devices[dn] + "_all")
    cd("Release")
    exec_script("~/ti/ccs1230/ccs/utils/bin/gmake clean")

    exec_script('sed -i "s/--define=HASH_ID=[0-9]/--define=HASH_ID=%d/" makefile' % hn)
    exec_script(
        'find . -name "subdir_rules.mk" -type f -print0 | xargs -0 sed -i\
          "s/--define=HASH_ID=[0-9]/--define=HASH_ID=%d/"'
        % hn
    )

    exec_script(
        'sed -i "s/--define=PROTOCOL_ID=[0-9]/--define=PROTOCOL_ID=%d/"\
          makefile'
        % an
    )
    exec_script(
        'find . -name "subdir_rules.mk" -type f -print0 | xargs -0 sed -i\
          "s/--define=PROTOCOL_ID=[0-9]/--define=PROTOCOL_ID=%d/"'
        % an
    )

    exec_script(
        'sed -i "s/--define=MEM_SIZE_LOG=[0-9]/--define=MEM_SIZE_LOG=%d/"\
          makefile'
        % asn
    )
    exec_script(
        'find . -name "subdir_rules.mk" -type f -print0 | xargs -0 sed -i\
          "s/--define=MEM_SIZE_LOG=[0-9]/--define=MEM_SIZE_LOG=%d/"'
        % asn
    )

    exec_script(
        'sed -i "s/--define=NO_COMPUTE=[0-1]/--define=NO_COMPUTE=%d/"\
          makefile'
        % (1 if NO_COMPUTE else 0)
    )
    exec_script(
        'find . -name "subdir_rules.mk" -type f -print0 | xargs -0 sed -i\
          "s/--define=NO_COMPUTE=[0-1]/--define=NO_COMPUTE=%d/"'
        % (1 if NO_COMPUTE else 0)
    )

    exec_script("~/ti/ccs1230/ccs/utils/bin/gmake -k -j 8 all -O")
    mem = parse_map(dn)
    mem = json.loads(mem)

    cd("../")

    if algos[an] == "baseline" or hashes[hn] == "emptyhash":
        t = {}
        cd("../")
    else:
        out = exec_script(
            'find . -name "*.out" | grep Release', stdout=subprocess.PIPE
        ).stdout.strip()
        target = exec_script(
            'find . -name "*.ccxml"', stdout=subprocess.PIPE
        ).stdout.strip()

        exec_script(
            "~/ti/uniflash_8.3.0/dslite.sh --config %s --flash %s --run" % (target, out)
        )
        time.sleep(3)

        cd("../")

        t = run_verifier(an, hn, dn, asn, NO_COMPUTE)
        try:
            t = json.loads(t)
        except Exception as e:
            print(t)
            print(e)
            return False
    print(t, mem)
    res = {"time": t, "mem": mem}
    os.makedirs("exppy/results", exist_ok=True)

    if not DEBUG:
        with open(outfile, "w") as outfile:
            json.dump(res, outfile)
    else:
        print(res)

    return True


def main():
    cd(root)

    # dn, asn, an, hn = 2, 6, 7, 2
    # run_experiment(dn, asn, an, hn, True, True)
    # return

    for dn in [2]:
        for asn in attest_size[dn]:
            for no_compute in [True, False]:
                for hn in supported[devices[dn]]:
                    for an in [0, 1, 2, 3, 4, 6]:
                        run_experiment(dn, asn, an, hn, no_compute)
                run_experiment(dn, asn, 5, 0, no_compute)
                run_experiment(dn, asn, 5, 6, no_compute)
                run_experiment(dn, asn, 7, 0, no_compute)
                run_experiment(dn, asn, 7, 6, no_compute)


if __name__ == "__main__":
    main()
