#! /usr/bin/env python3 -u

import os
import json
from copy import deepcopy
from statistics import mean
import pandas as pd

pd.set_option("display.max_rows", None)
pd.set_option("display.max_columns", None)
pd.set_option("display.width", None)
# pd.set_option('display.precision', 3)


def load_data():
    data = {}
    data_no = {}
    for fname in os.listdir("results"):
        h, algo, dev, msize, no_compute = fname.split(".")[0].split("-")
        if no_compute == "no_compute":
            cdata = data_no
        else:
            cdata = data

        if dev not in cdata:
            cdata[dev] = {}
        if msize not in cdata[dev]:
            cdata[dev][msize] = {}
        if algo not in cdata[dev][msize]:
            cdata[dev][msize][algo] = {}

        with open("results/" + fname) as ff:
            fd = json.loads(ff.read())
        cdata[dev][msize][algo][h] = fd
    return data, data_no


# def clean_data_old(data):
#     data = deepcopy(data)

#     obj_map = {'sha256': 'SHA_256'}
#     for dev in data:
#         for msize in data[dev]:
#             for algo in list(data[dev][msize].keys()):
#                 if algo == 'pose_db_random':
#                     data[dev][msize][algo]['none'] = \
#                         data[dev][msize][algo]['sha256']

#                     del data[dev][msize][algo]['sha256']
#                 for h in data[dev][msize][algo]:
#                     cur = data[dev][msize][algo][h]
#                     if algo.startswith('pose_db'):
#                         if 'query_time' in cur['time']:
#                             ll = cur['time']['query_time']
#                             cur['time']['query_time'] = mean(ll)
#                         # else: print('missing query_time:', dev, algo, h)
#                     mem_algo = cur['mem'][algo + '.obj']
#                     if algo == 'perito':
#                         hname = 'hmac'
#                     elif h in obj_map:
#                         hname = obj_map[h]
#                     else:
#                         hname = h
#                     if algo != 'pose_db_random':
#                         mem_hash = cur['mem'][hname + '.obj']
#                         cur['mem'] = {'algo': mem_algo, 'hash': mem_hash}
#                     else:
#                         cur['mem'] = {'algo': mem_algo}
#                     # print(dev, algo, h, cur)

#     return data


def clean_data(data, data_no):
    data = deepcopy(data)

    # obj_map = {'sha256': 'SHA_256'}
    for dev in data:
        for msize in data[dev]:
            for algo in list(data[dev][msize].keys()):
                if algo == "baseline":
                    continue
                if algo == "pose_db_random":
                    data[dev][msize][algo]["none"] = data[dev][msize][algo]["sha256"]
                    data_no[dev][msize][algo]["none"] = data_no[dev][msize][algo][
                        "sha256"
                    ]

                    del data[dev][msize][algo]["sha256"]
                for h in data[dev][msize][algo]:
                    if h == "emptyhash":
                        continue

                    cur = data[dev][msize][algo][h]

                    if algo.startswith("pose_db"):
                        if "query_time" in cur["time"]:
                            ll = cur["time"]["query_time"]
                            cur["time"]["query_time"] = mean(ll)
                        # else: print('missing query_time:', dev, algo, h)

                    if algo != "pose_db_random":
                        if algo == "perito":
                            mem_algo = (
                                data[dev][msize]["perito"]["emptyhash"]["mem"]["total"]
                                - data[dev][msize]["baseline"]["emptyhash"]["mem"][
                                    "total"
                                ]
                            )
                        else:
                            mem_algo = (
                                cur["mem"]["total"]
                                - data[dev][msize]["baseline"][h]["mem"]["total"]
                            )
                        mem_hash = (
                            cur["mem"]["total"]
                            - data[dev][msize][algo]["emptyhash"]["mem"]["total"]
                        )
                        if mem_hash < 0 or mem_algo < 0:
                            print(
                                dev,
                                algo,
                                h,
                                data[dev][msize][algo][h]["mem"]["total"],
                                data[dev][msize][algo]["emptyhash"]["mem"]["total"],
                                cur["mem"]["total"],
                                data[dev][msize]["baseline"][h]["mem"]["total"],
                            )
                        assert mem_hash >= 0 and mem_algo >= 0
                        cur["mem"] = {"algo": mem_algo, "hash": mem_hash}
                        assert (
                            cur["time"]["total_time"] + 0.2
                            >= data_no[dev][msize][algo][h]["time"]["total_time"]
                        ), (
                            dev,
                            msize,
                            algo,
                            h,
                            cur["time"]["total_time"],
                            data_no[dev][msize][algo][h]["time"]["total_time"],
                        )
                        cur["time"]["compute_time"] = (
                            cur["time"]["total_time"]
                            - data_no[dev][msize][algo][h]["time"]["total_time"]
                        )
                        # this is probably needed to cope for small errors in measurement
                        if cur["time"]["compute_time"] < 0:
                            cur["time"]["compute_time"] = 0
                    else:
                        mem_algo = (
                            cur["mem"]["total"]
                            - data[dev][msize]["baseline"]["emptyhash"]["mem"]["total"]
                        )
                        assert mem_algo >= 0
                        cur["mem"] = {"algo": mem_algo}

    for dev in data:
        for msize in data[dev]:
            if "baseline" in data[dev][msize]:
                del data[dev][msize]["baseline"]
            for algo in data[dev][msize]:
                if "emptyhash" in data[dev][msize][algo]:
                    del data[dev][msize][algo]["emptyhash"]
    return data


def clean_nan(s):
    return s.replace("<NA>", "---").replace("nan", "---")


def make_graphics(data):

    trans = {
        "pose_db_random": "\\poserandom{}",
        "pose_db": "\\posegraph{}",
        "pose_db_light": "\\poselight{}",
        "dziembowski": "\\dziembowski{}",
        "perito": "\\perito{}",
        "karvelas": "\\karvelas{}",
        "karame": "\\karame{}",
    }

    for dev in data:
        for msize in data[dev]:
            dd = {}
            for algo in data[dev][msize]:
                dd[trans[algo]] = {}
                for h in data[dev][msize][algo]:
                    dd[trans[algo]][h] = {}
                    cur = data[dev][msize][algo][h]
                    dd[trans[algo]][h] = cur["time"]["total_time"]
            dd = pd.DataFrame.from_dict(dd)
            dd = dd.reindex(sorted(dd.columns), axis=1)
            dd.sort_index(inplace=True)
            dd = dd.transpose()
            with open("results_latex/total_time_%s_%s.tex" % (dev, msize), "w") as ff:
                print(clean_nan(dd.style.format(precision=1).to_latex()), file=ff)

    for dev in data:
        for msize in data[dev]:
            dd = {}
            for algo in data[dev][msize]:
                dd[trans[algo]] = {}
                for h in data[dev][msize][algo]:
                    cur = data[dev][msize][algo][h]
                    if "compute_time" in cur["time"]:
                        dd[trans[algo]][h] = cur["time"]["compute_time"]
            dd = pd.DataFrame.from_dict(dd)
            dd = dd.reindex(sorted(dd.columns), axis=1)
            dd.sort_index(inplace=True)
            dd = dd.transpose()
            with open("results_latex/compute_time_%s_%s.tex" % (dev, msize), "w") as ff:
                print(clean_nan(dd.style.format(precision=1).to_latex()), file=ff)

    for dev in data:
        for msize in data[dev]:
            dd = {}
            for algo in data[dev][msize]:
                if algo.startswith("pose_db"):
                    dd[trans[algo]] = {}
                    for h in data[dev][msize][algo]:
                        cur = data[dev][msize][algo][h]
                        if "query_time" in cur["time"]:
                            dd[trans[algo]][h] = cur["time"]["query_time"]
            dd = pd.DataFrame.from_dict(dd)
            dd = dd.reindex(sorted(dd.columns), axis=1)
            dd.sort_index(inplace=True)
            dd = dd.transpose()
            with open("results_latex/query_time_%s_%s.tex" % (dev, msize), "w") as ff:
                print(clean_nan(dd.style.format(precision=3).to_latex()), file=ff)

    for dev in data:
        for msize in data[dev]:
            dd = {}
            for algo in data[dev][msize]:
                dd[trans[algo]] = {}
                for h in data[dev][msize][algo]:
                    cur = data[dev][msize][algo][h]
                    dd[trans[algo]][h] = cur["mem"]["algo"]
            dd = pd.DataFrame.from_dict(dd)
            for algo in data[dev][msize]:
                dd[trans[algo]] = dd[trans[algo]].astype("Int64")
            dd = dd.reindex(sorted(dd.columns), axis=1)
            dd.sort_index(inplace=True)
            dd = dd.transpose()
            with open("results_latex/mem_algo_%s_%s.tex" % (dev, msize), "w") as ff:
                print(clean_nan(dd.style.to_latex()), file=ff)

    for dev in data:
        for msize in data[dev]:
            dd = {}
            for algo in data[dev][msize]:
                dd[trans[algo]] = {}
                for h in data[dev][msize][algo]:
                    cur = data[dev][msize][algo][h]
                    if "hash" in cur["mem"]:
                        dd[trans[algo]][h] = cur["mem"]["hash"]
            dd = pd.DataFrame.from_dict(dd)
            dd[trans["pose_db_random"]] = dd[trans["pose_db_random"]].astype("Int64")
            dd[trans["perito"]] = dd[trans["perito"]].astype("Int64")
            dd = dd.reindex(sorted(dd.columns), axis=1)
            dd.sort_index(inplace=True)
            dd = dd.transpose()
            with open("results_latex/mem_hash_%s_%s.tex" % (dev, msize), "w") as ff:
                print(clean_nan(dd.style.to_latex()), file=ff)

    dd = {}
    for dev in data:
        dd[dev] = {}
        for msize in data[dev]:
            if msize == "6":
                for algo in data[dev][msize]:
                    dd[dev][trans[algo]] = []
                    for h in data[dev][msize][algo]:
                        cur = data[dev][msize][algo][h]
                        dd[dev][trans[algo]] += [cur["mem"]["algo"]]
                    dd[dev][trans[algo]] = sum(dd[dev][trans[algo]]) // len(
                        dd[dev][trans[algo]]
                    )
    dd = pd.DataFrame().from_dict(dd)
    for msize in data[dev]:
        if msize == 6:
            for algo in data[dev][msize]:
                dd[dev][trans[algo]] = dd[dev][trans[algo]].astype("Int64")
    dd = dd.reindex(sorted(dd.columns), axis=1)
    dd.sort_index(inplace=True)
    dd = dd.transpose()
    with open("results_latex/mem_algo.tex", "w") as ff:
        print(clean_nan(dd.style.to_latex()), file=ff)

    dd = {}
    for dev in data:
        dd[dev] = {}
        for msize in data[dev]:
            if msize == "6":
                for algo in data[dev][msize]:
                    if algo == "pose_db_random" or algo == "perito":
                        continue
                    for h in data[dev][msize][algo]:
                        cur = data[dev][msize][algo][h]
                        if h not in dd[dev]:
                            dd[dev][h] = []
                        if "hash" in cur["mem"]:
                            dd[dev][h] += [cur["mem"]["hash"]]
    for dev in data:
        for h in list(dd[dev].keys()):
            dd[dev][h] = sum(dd[dev][h]) // len(dd[dev][h])
    dd = pd.DataFrame.from_dict(dd, dtype="Int64")
    dd = dd.reindex(sorted(dd.columns), axis=1)
    dd.sort_index(inplace=True)
    dd = dd.transpose()
    with open("results_latex/mem_hash.tex", "w") as ff:
        print(clean_nan(dd.style.to_latex()), file=ff)


def make_plot(data):
    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd

    colors = ["blue", "green", "red", "purple", "orange", "pink", "magenta"]
    plt.rcParams['text.usetex'] = True
    trans = {
        "pose_db_random": r"\textbf{PoSE\textsubscript{random}}",
        "pose_db": r"\textbf{PoSE\textsubscript{graph}}",
        "pose_db_light": r"\textbf{PoSE\textsubscript{light}}",
        "dziembowski": r"\textbf{DFKP}",
        "perito": r"\textbf{PT}",
        "karvelas": r"\textbf{KK}",
        "karame": r"\textbf{KL}",
    }

    for dev in data:
        for msize in data[dev]:
            dd = {}
            for algo in data[dev][msize]:
                for h in data[dev][msize][algo]:
                    cur = data[dev][msize][algo][h]
                    if "compute_time" in cur["time"]:
                        if h not in dd:
                            dd[h] = {}
                        dd[h][trans[algo]] = cur["time"]["compute_time"]

            df = pd.DataFrame(dd)
            df = df.reindex(sorted(df.columns), axis=1)
            plt.figure(figsize=(10, 6))

            vals = [v for p in df.index for v in df.loc[p] if not np.isnan(v)]
            extra = (max(vals) - min(vals)) / 100

            n_hashes = len(df.columns)
            n_protocols = len(df.index)
            bar_width = 0.8 / n_protocols
            index = np.arange(n_hashes)
            for j in index:
                ji = 0
                for i, protocol in enumerate(sorted(df.index)):
                    if not np.isnan(df.loc[protocol].iloc[j]):
                        xval = j + ji * bar_width
                        plt.bar(
                            xval,
                            df.loc[protocol].iloc[j] + extra,
                            width=bar_width,
                            label=protocol,
                            color=colors[i],
                            bottom=-extra,
                        )
                        yval = df.loc[protocol].iloc[j]
                        plt.text(xval, yval, '%.1f' % yval, ha='center', va='bottom')
                        ji += 1
            plt.xlabel('Hash functions')
            plt.ylabel("Computation Time (seconds)")
            # plt.title('Computation Time by Hash Function and Hash Function')
            plt.xticks(
                index + 0.2 + bar_width / n_protocols * (n_protocols - 1) / 2,
                df.columns,
            )
            handles, labels = plt.gca().get_legend_handles_labels()
            by_label = dict(zip(labels, handles))
            plt.legend(by_label.values(), by_label.keys(), title="")
            # plt.legend(title='')
            plt.ylim(bottom=-extra)

            plt.tight_layout()
            # plt.show()
            plt.savefig(f"results_latex/compute_time_{dev}_{msize}.pdf", format="pdf")


    for dev in data:
        for msize in data[dev]:
            dd = {}
            for algo in data[dev][msize]:
                for h in data[dev][msize][algo]:
                    cur = data[dev][msize][algo][h]
                    if "total_time" in cur["time"]:
                        if h not in dd:
                            dd[h] = {}
                        dd[h][trans[algo]] = cur["time"]["total_time"]

            df = pd.DataFrame(dd)
            df = df.reindex(sorted(df.columns), axis=1)
            plt.figure(figsize=(16, 6))

            vals = [v for p in df.index for v in df.loc[p] if not np.isnan(v)]
            extra = (max(vals) - min(vals)) / 100

            n_hashes = len(df.columns)
            n_protocols = len(df.index)
            bar_width = 0.8 / n_protocols
            index = np.arange(n_hashes)
            for j in index:
                ji = 0
                for i, protocol in enumerate(sorted(df.index)):
                    if not np.isnan(df.loc[protocol].iloc[j]):
                        xval = j + ji * bar_width
                        plt.bar(
                            xval,
                            df.loc[protocol].iloc[j] + extra,
                            width=bar_width,
                            label=protocol,
                            color=colors[i],
                            bottom=-extra,
                        )
                        yval = df.loc[protocol].iloc[j]
                        plt.text(xval, yval, '%.1f' % yval, ha='center', va='bottom')
                        ji += 1
            plt.xlabel('Hash functions')
            plt.ylabel("Total Time (seconds)")
            # plt.title('Computation Time by Hash Function and Hash Function')
            plt.xticks(
                index + 0.2 + bar_width / n_protocols * (n_protocols - 1) / 2,
                df.columns,
            )
            handles, labels = plt.gca().get_legend_handles_labels()
            by_label = dict(zip(labels, handles))
            plt.legend(by_label.values(), by_label.keys(), title="")
            # plt.legend(title='')
            plt.ylim(bottom=-extra)

            plt.tight_layout()
            # plt.show()
            plt.savefig(f"results_latex/total_time_{dev}_{msize}.pdf", format="pdf")

    for dev in data:
        if dev == "CC2652":
            dd = {}
            for msize in data[dev]:

                for algo in data[dev][msize]:
                    for h in data[dev][msize][algo]:
                        proceed = False
                        if algo == "perito":
                            if h == "sha256":
                                proceed = True
                        elif algo == "pose_db_random":
                            if h == "none":
                                proceed = True
                        else:
                            if h == "sha256hw":
                                proceed = True
                        if proceed:
                            cur = data[dev][msize][algo][h]
                            rsize = f"{2 ** (int(msize) - 5)}KB"
                            if rsize not in dd:
                                dd[rsize] = {}
                            ctime = (
                                cur["time"]["compute_time"]
                                if "compute_time" in cur["time"]
                                else 0
                            )
                            ttime = cur["time"]["total_time"]
                            dd[rsize][trans[algo]] = (ttime - ctime, ctime)

            df = pd.DataFrame(dd)
            df = df.reindex(sorted(df.columns), axis=1)
            plt.figure(figsize=(10, 6))

            n_hashes = len(df.columns)
            n_protocols = len(df.index)
            bar_width = 0.8 / n_protocols
            index = np.arange(n_hashes)
            for j in index:
                ji = 0
                for i, protocol in enumerate(sorted(df.index)):
                    if not np.isnan(df.loc[protocol].iloc[j][0]):
                        xval = j + ji * bar_width
                        plt.bar(
                            xval,
                            df.loc[protocol].iloc[j][0],
                            width=bar_width,
                            label=protocol,
                            color=colors[i],
                            alpha=0.7,
                            hatch='//',
                        )
                        plt.bar(
                            xval,
                            df.loc[protocol].iloc[j][1],
                            width=bar_width,
                            label=protocol,
                            color=colors[i],
                            bottom=df.loc[protocol].iloc[j][0],
                        )
                        yval = df.loc[protocol].iloc[j][0] + df.loc[protocol].iloc[j][1]
                        plt.text(xval, yval, '%.1f' % yval, ha='center', va='bottom')
                        ji += 1
            plt.ylim(0, plt.gca().get_ylim()[1] + 1)
            # plt.xlabel('Hash functions')
            plt.ylabel("Communication + Computation Time (seconds)")
            # plt.title('Computation Time by Hash Function and Hash Function')
            plt.xticks(
                index + 0.2 + bar_width / n_protocols * (n_protocols - 1) / 2,
                df.columns,
            )
            handles, labels = plt.gca().get_legend_handles_labels()
            by_label = dict(zip(labels, handles))
            plt.legend(by_label.values(), by_label.keys(), title="")
            # plt.legend(title='')

            plt.tight_layout()
            # plt.show()
            plt.savefig(f"results_latex/total_time_best_hash_{dev}.pdf", format="pdf")

    for dev in data:
        if dev == "CC2652":
            dd = {}
            for msize in data[dev]:

                for algo in data[dev][msize]:
                    for h in data[dev][msize][algo]:
                        proceed = False
                        if algo == "perito":
                            if h == "sha256":
                                proceed = True
                        elif algo == "pose_db_random":
                            pass
                        else:
                            if h == "sha256hw":
                                proceed = True
                        if proceed:
                            cur = data[dev][msize][algo][h]
                            rsize = f"{2 ** (int(msize) - 5)}KB"
                            if rsize not in dd:
                                dd[rsize] = {}
                            dd[rsize][trans[algo]] = cur["time"]["compute_time"]

            df = pd.DataFrame(dd)
            df = df.reindex(sorted(df.columns), axis=1)
            plt.figure(figsize=(10, 6))

            vals = [v for p in df.index for v in df.loc[p] if not np.isnan(v)]
            extra = (max(vals) - min(vals)) / 100

            n_hashes = len(df.columns)
            n_protocols = len(df.index)
            bar_width = 0.8 / n_protocols
            index = np.arange(n_hashes)
            for j in index:
                ji = 0
                for i, protocol in enumerate(sorted(df.index)):
                    if not np.isnan(df.loc[protocol].iloc[j]):
                        xval = j + ji * bar_width
                        yval = df.loc[protocol].iloc[j]
                        plt.bar(
                            xval,
                            yval + extra,
                            width=bar_width,
                            label=protocol,
                            color=colors[i],
                            bottom=-extra,
                        )
                        plt.text(xval, yval, '%.1f' % yval, ha='center', va='bottom')
                        ji += 1
            # plt.xlabel('Hash functions')
            plt.ylabel("Computation Time (seconds)")
            # plt.title('Computation Time by Hash Function and Hash Function')
            plt.xticks(
                index + 0.2 + bar_width / n_protocols * (n_protocols - 1) / 2,
                df.columns,
            )
            handles, labels = plt.gca().get_legend_handles_labels()
            by_label = dict(zip(labels, handles))
            plt.legend(by_label.values(), by_label.keys(), title="")
            # plt.legend(title='')

            plt.tight_layout()
            plt.ylim(bottom=-extra)
            # plt.show()
            plt.savefig(f"results_latex/compute_time_best_hash_{dev}.pdf", format="pdf")

    dd = {}
    for dev in data:

        for msize in data[dev]:
            if msize == "6":
                for algo in data[dev][msize]:
                    for h in data[dev][msize][algo]:
                        proceed = False
                        if algo == "perito":
                            if h == "sha256":
                                proceed = True
                        elif algo == "pose_db_random":
                            if h == "none":
                                proceed = True
                        else:
                            if dev == "CC2652" and h == "sha256hw":
                                proceed = True
                            elif dev == "F5529" and h == "blake3":
                                proceed = True
                            elif dev == "FR5994" and h == "aeshash":
                                proceed = True
                        if proceed:
                            cur = data[dev][msize][algo][h]
                            if dev not in dd:
                                dd[dev] = {}
                            ctime = (
                                cur["time"]["compute_time"]
                                if "compute_time" in cur["time"]
                                else 0
                            )
                            ttime = cur["time"]["total_time"]
                            dd[dev][trans[algo]] = (ttime - ctime, ctime)

    df = pd.DataFrame(dd)
    df = df.reindex(["F5529", "FR5994", "CC2652"], axis=1)
    plt.figure(figsize=(10, 6))

    n_hashes = len(df.columns)
    n_protocols = len(df.index)
    bar_width = 0.8 / n_protocols
    index = np.arange(n_hashes)
    for j in index:
        ji = 0
        for i, protocol in enumerate(sorted(df.index)):
            if not np.isnan(df.loc[protocol].iloc[j][0]):
                xval = j + ji * bar_width
                plt.bar(
                    xval,
                    df.loc[protocol].iloc[j][0],
                    width=bar_width,
                    label=protocol,
                    color=colors[i],
                    alpha=0.7,
                    hatch='//',
                )
                plt.bar(
                    xval,
                    df.loc[protocol].iloc[j][1],
                    width=bar_width,
                    label=protocol,
                    color=colors[i],
                    bottom=df.loc[protocol].iloc[j][0],
                )
                yval = df.loc[protocol].iloc[j][0] + df.loc[protocol].iloc[j][1]
                plt.text(xval, yval, '%.1f' % yval, ha='center', va='bottom')
                ji += 1
    # plt.xlabel('Hash functions')
    plt.ylabel("Communication + Computation Time (seconds)")
    # plt.title('Computation Time by Hash Function and Hash Function')
    plt.xticks(
        index + 0.2 + bar_width / n_protocols * (n_protocols - 1) / 2,
        df.columns,
    )
    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(), title="")
    # plt.legend(title='')

    plt.tight_layout()
    # plt.show()
    plt.savefig("results_latex/total_time_best_hash_2KB.pdf", format="pdf")


if __name__ == "__main__":
    data, data_no = load_data()
    data = clean_data(data, data_no)
    make_graphics(data)
    make_plot(data)
