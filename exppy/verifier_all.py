#! /usr/bin/env python3 -u

import random
import json
from timeit import default_timer as timer

from hashlib import sha256, blake2s
from blake3 import blake3
from lib.hash.asconwrapper import ascon
from lib.hash.aeshash import aeshash


from lib.proto import (
    pose_db,
    dziembowski,
    karvelas,
    karame,
    perito,
    pose_db_light,
    pose_db_random,
)


random.seed(0)

DEBUG = False

ATTEST_SIZE_LOG = 7
ATTEST_SIZE = 2**ATTEST_SIZE_LOG

DEVICE_ID = 2
HASH_ID = 4
PROTOCOL_ID = 1
NO_COMPUTE = True

device_list = ["F5529", "FR5994", "CC2652"]


hash_list = [sha256, blake2s, blake3, ascon, aeshash, sha256]
h = hash_list[HASH_ID]

protocol_list = [
    pose_db.pose_db,
    dziembowski.dziembowski,
    None,
    karvelas.karvelas,
    karame.karame,
    perito.perito,
    pose_db_light.pose_db_light,
    pose_db_random.pose_db_random,
]

protocol = protocol_list[PROTOCOL_ID]


def ble_device():

    import asyncio
    from bleak import BleakClient, BleakScanner
    from bleak.backends.characteristic import BleakGATTCharacteristic
    from bleak.backends.device import BLEDevice
    from bleak.backends.scanner import AdvertisementData

    UART_SERVICE_UUID = "f000c0c0-0451-4000-b000-000000000000"
    UART_TX_CHAR_UUID = "f000c0c2-0451-4000-b000-000000000000"
    UART_RX_CHAR_UUID = "f000c0c1-0451-4000-b000-000000000000"

    received_data = [b""]
    # serverMACAddress = "98:D3:51:F6:31:AB"

    async def run_protocol():

        p = protocol(ATTEST_SIZE, h)

        if DEBUG:
            print(
                "Running protocol with m=%d" % p.m,
                "rounds=%d" % p.rounds if "rounds" in p.__dict__ else "",
            )

        def match_nus_uuid(device: BLEDevice, adv: AdvertisementData):
            if UART_SERVICE_UUID.lower() in adv.service_uuids:
                return True

            return False

        device = await BleakScanner.find_device_by_filter(match_nus_uuid)

        received = asyncio.Event()

        def handle_rx(_: BleakGATTCharacteristic, data: bytearray):
            received_data[0] = bytes(data)
            received.set()

        async with BleakClient(device) as client:
            await client.start_notify(UART_TX_CHAR_UUID, handle_rx)

            nus = client.services.get_service(UART_SERVICE_UUID)
            rx_char = nus.get_characteristic(UART_RX_CHAR_UUID)

            lastmsg = None
            finished = None
            t0, t1, t2 = timer(), 0, 0
            tt = []
            while finished is None:
                step = p.run_protocol_verifier(lastmsg, NO_COMPUTE)
                if DEBUG:
                    print(step)
                if isinstance(step, bool):
                    finished = step
                else:
                    msg, resp_size = step
                    if msg is not None:
                        t1 = timer()
                        await client.write_gatt_char(rx_char, msg)
                    if resp_size is not None:
                        await received.wait()
                        lastmsg = received_data[0]
                        received.clear()

                        t2 = timer()
                        tt.append((t1, t2))
                        if DEBUG:
                            print(lastmsg)
            tt.append((t0, t2))

        data = p.parse_timings(tt)
        assert finished
        print(json.dumps(data))

    asyncio.run(run_protocol())


def bt_device():
    import serial

    # needs first, should be done in python
    # F5529
    # sudo rfcomm connect /dev/rfcomm0 98:D3:21:F7:D5:95
    # FR5994
    # sudo rfcomm connect /dev/rfcomm0 98:D3:51:F6:31:AB

    SERIAL_PORT = "/dev/rfcomm0"

    def run_protocol():

        p = protocol(ATTEST_SIZE, h)

        if DEBUG:
            print(
                "Running protocol with m=%d" % p.m,
                "rounds=%d" % p.rounds if "rounds" in p.__dict__ else "",
            )

        ser = serial.Serial(SERIAL_PORT)  # open serial port

        lastmsg = None
        finished = None
        t0, t1, t2 = timer(), 0, 0
        tt = []
        while finished is None:
            step = p.run_protocol_verifier(lastmsg, NO_COMPUTE)
            if DEBUG:
                print(step)
            if isinstance(step, bool):
                finished = step
            else:
                msg, resp_size = step
                if msg is not None:
                    t1 = timer()
                    ser.write(msg)
                if resp_size is not None:
                    # message ended by '\n'
                    if resp_size == 0:
                        lastmsg = ser.readline()
                    else:
                        lastmsg = ser.read(resp_size)
                    t2 = timer()
                    tt.append((t1, t2))
                    if DEBUG:
                        print(lastmsg)
        tt.append((t0, t2))
        data = p.parse_timings(tt)
        assert finished
        print(json.dumps(data))

    run_protocol()


if __name__ == "__main__":
    if DEVICE_ID == 2:
        ble_device()
    elif DEVICE_ID in [0, 1]:
        bt_device()
