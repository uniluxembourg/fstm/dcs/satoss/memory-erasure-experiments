# Memory Erasure Experiments

The entry point for the experiments is the script `exppy/experiments.py`. It assumes projects present in folders `prover_*DEVICE*_all` have been imported and compiled in Release mode in `Code Composer Studio`. The script `exppy/graphics.py` can be used to generate `latex` compatible results.
